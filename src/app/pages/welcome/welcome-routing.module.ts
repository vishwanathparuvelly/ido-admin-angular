import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './welcome.component';

const routes: Routes = [
  {
    path: '', component: WelcomeComponent,
    children: [
      { path: '', redirectTo: 'admin', pathMatch: 'prefix' },
      {
        path: 'admin',
        loadChildren: () =>
          import('./admin/admin.module').then((m) => m.AdminModule),
      },
      {
        path: 'vendors',
        loadChildren: () =>
          import('./vendors/vendors.module').then((m) => m.VendorsModule),
      },
      {
        path: 'categories',
        loadChildren: () =>
          import('./categories/categories.module').then((m) => m.CategoriesModule),
      },
      {
        path: 'wholesale-categories',
        loadChildren: () =>
          import('./wholesale-categories/wholesale-categories.module').then((m) => m.WholesaleCategoriesModule),
      },
      {
        path: 'buyers',
        loadChildren: () =>
          import('./buyers/buyers.module').then((m) => m.BuyersModule),
      },
      {
        path: 'products',
        loadChildren: () =>
          import('./products/products.module').then((m) => m.ProductsModule),
      },
      {
        path: 'wholesale-products',
        loadChildren: () =>
          import('./wholesale-products/wholesale-products.module').then((m) => m.WholesaleProductsModule),
      },
      {
        path: 'coupons',
        loadChildren: () =>
          import('./coupons/coupons.module').then((m) => m.CouponsModule),
      },
      {
        path: 'banners',
        loadChildren: () =>
          import('./banners/banners.module').then((m) => m.BannersModule),
      },
      {
        path: 'order',
        loadChildren: () =>
          import('./orders/order/order.module').then((m) => m.OrderModule),
      },
      {
        path: 'wholesale-orders',
        loadChildren: () =>
          import('./wholesale-orders/wholesale-orders.module').then((m) => m.WholesaleOrdersModule),
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WelcomeRoutingModule { }
