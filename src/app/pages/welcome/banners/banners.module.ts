import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BannersRoutingModule } from './banners-routing.module';
import { BannersComponent } from '../banners/banners.component';
import { AccountSharedModule } from 'src/app/accounts.shared.module';


@NgModule({
  declarations: [BannersComponent],
  imports: [
    CommonModule,
    BannersRoutingModule,
    AccountSharedModule
  ]
})
export class BannersModule { }
