import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AppService } from 'src/app/app.service';
import { NzUploadChangeParam, NzUploadFile } from 'ng-zorro-antd/upload';

export interface Banners {
  'created_on': string;
  '_id': string;
  'status': string;
  'name': string;
  'image': string;
}

function getBase64(file: File): Promise<string | ArrayBuffer | null> {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

@Component({
  selector: 'app-banners',
  templateUrl: './banners.component.html',
  styleUrls: ['./banners.component.css']
})
export class BannersComponent implements OnInit {

  selectedTab = 0;
  isTableLoading = false;
  selectedId = '';
  isVisible = false;
  categoryType = 1;
  bannerName = '';
  updateId: any;
  isVisibleUpdate = false;
  tableList: Banners[] = [];
  currentPage = 1;
  limit = this.appService.limit;
  nzPageSizeOptions = this.appService.nzPageSizeOptions;
  count: any = 0;
  accountParamsobj: any = {
    limit: this.limit,
    pageNumber: this.currentPage
  };
  view = -1;
  fileList!: File;
  previewImage: string | undefined = '';
  previewVisible = false;
  files!: NzUploadFile;

  // handlePreview = async (file: NzUploadFile) => {
  //   console.log('file: ', file);
  //   if (!file.url && !file.preview) {
  //     file.preview = await getBase64(file.originFileObj!);
  //   }
  //   this.files = file;
  //   console.log('this.files: ', this.files);
  //   this.previewImage = file.url || file.preview;
  //   this.previewVisible = true;
  // }

  constructor(
    public appService: AppService,
    private nzMessage: NzMessageService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.getTableList();
  }

  getTableList(): any {
    this.accountParamsobj.limit = this.limit;
    this.accountParamsobj.pageNumber = this.currentPage - 1;
    this.isTableLoading = true;
    let url = '';
    if (this.selectedTab === 0) {
      url = 'banners/page/' + this.accountParamsobj.pageNumber + '/' + this.accountParamsobj.limit;
    } else if (this.selectedTab === 1) {
      url = 'wholesale_banners/page/' + this.accountParamsobj.pageNumber + '/' + this.accountParamsobj.limit;
    }
    try {
      this.appService.getMethod(url)
        .subscribe((resp: any) => {
          console.log(resp);
          this.tableList = resp.data;
          console.log('this.tableList: ', this.tableList);
          this.count = resp.count;
          this.isTableLoading = false;
        },
          (resp: any) => {
            console.log('err', resp);
            this.isTableLoading = false;
            this.nzMessage.error(resp.name, resp.statusText);
          });
    } catch (e) {
    }
  }

  onNextPage(event: number): any {
    console.log('next page ', event);
    this.view = -1;
    this.currentPage = event;
    this.getTableList();
  }

  onLimitChange(event: any): any {
    this.limit = event;
    this.currentPage = 1;
    this.getTableList();
  }

  onExpandChange(index: number, checked: boolean): void {
    if (checked) {
      this.view = index;
      // this.getInfo(this.tableList[index]._id);
    } else {
      this.view = -1;
    }
  }

  cancel(): any { }

  confirmCategory(data: any, type: number): any {
    this.categoryType = type;
    let url = '';
    if (this.selectedTab === 0) {
      url = 'banners/' + data._id + '/delete';
    } else if (this.selectedTab === 1) {
      url = 'wholesale_banners/' + data._id + '/delete';
    }
    try {
      this.appService.patchMethod(url)
        .subscribe((resp: any) => {
          console.log(resp);
          if (resp.status === 200) {
            this.nzMessage.success(resp.body.additional_info);
            this.getTableList();
          }
        },
          (resp: any) => {
            console.log('err', resp);
            this.nzMessage.error(resp.name, resp.statusText);
          });
    } catch (e) {
    }
  }

  tabChange(event: number): any {
    this.selectedTab = event;
    this.selectedId = '';
    this.getTableList();
  }

  create(): any {
    // this.categoryType = type;
    // this.actionType = action;
    // if (data) {
    //   this.categoryName = data.name;
    //   this.updateId = data._id;
    //   this.isVisibleUpdate = true;
    // } else {
    // }
    this.isVisible = true;
  }

  handleOk(): void {
    let url = '';
    let body;
    if (this.selectedTab === 0) {
      url = 'banners/new';
    } else if (this.selectedTab === 1) {
      url = 'wholesale_banners/new';
    }
    body = { name: this.bannerName };
    console.log('this.fileList: ', this.fileList);
    try {
      this.appService.fileMethod(url, body, this.fileList)
        .subscribe((resp: any) => {
          console.log(resp);
          this.isVisible = false;
          if (resp.status === 200) {
            this.bannerName = '';
            this.nzMessage.success(resp.body.additional_info);
            this.getTableList();
          }
        },
          (err: any) => {
            console.log('err', err);
            this.nzMessage.error(err.name, err.statusText);
          });
    } catch (e) {
    }
  }

  handleCancel(): void {
    console.log('Button cancel clicked!');
    this.isVisible = false;
  }

  onChangeStatus(data: any): any {
    console.log('data', data);
    let url = '';
    if (this.selectedTab === 0) {
      url = 'banners/' + data._id + '/update_status';
    } else if (this.selectedTab === 1) {
      url = 'wholesale_banners/' + data._id + '/update_status';
    }
    try {
      this.appService.postMethod(url, { status: data.status === 'INACTIVE' ? 'ACTIVE' : 'INACTIVE' })
        .subscribe(resp => {
          if (resp.success) {
            this.nzMessage.success(resp.body.additional_info);
            this.getTableList();
          } else {
          }
        },
          (err: any) => {
            this.getTableList();
            this.nzMessage.error(err.name, err.statusText);
          });
    } catch (e) { }
  }

  handleChange({ file, fileList }: any): void {
    console.log('file: ', file);
    const status = file.status;
    if (status !== 'uploading') {
      console.log(file, fileList);
    }
    this.fileList = file;
  }
}
