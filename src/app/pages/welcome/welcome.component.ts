import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  isCollapsed = false;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  logOut() {
    this.router.navigateByUrl('/login');
    sessionStorage.clear();
  }

}
