import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VendorsRoutingModule } from './vendors-routing.module';
import { VendorsComponent } from './vendors.component';
import { AccountSharedModule } from 'src/app/accounts.shared.module';
import { CreateVendorComponent } from './create-vendor/create-vendor.component';


@NgModule({
  declarations: [VendorsComponent],
  imports: [
    CommonModule,
    VendorsRoutingModule,
    AccountSharedModule
  ]
})
export class VendorsModule { }
