import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VendorsComponent } from './vendors.component';

const routes: Routes = [
  {
    path: '', component: VendorsComponent,
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'prefix' },
      {
        path: 'list', loadChildren: () => import('./list-vendors/list-vendors.module').then((m) => m.ListVendorsModule)
      },
      {
        path: 'create', loadChildren: () => import('./create-vendor/create-vendor.module').then((m) => m.CreateVendorModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorsRoutingModule { }
