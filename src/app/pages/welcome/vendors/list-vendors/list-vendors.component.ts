import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AppService } from 'src/app/app.service';

export interface Address {
  'address_line1': string;
  'address_line2': string;
  'city': string;
  'state': string;
  'landmark': string;
  'pincode': string;
}

export interface VendorInfo {
  'company_address': Address;
  'account_status': string;
  'created_on': string;
  '_id': string;
  'email': string;
  'phone_number': string;
  'name': string;
  'company_name': string;
  'GSTIN': string;
  'opening_time': string;
  'closing_time': string;
}

@Component({
  selector: 'app-list-vendors',
  templateUrl: './list-vendors.component.html',
  styleUrls: ['./list-vendors.component.css']
})
export class ListVendorsComponent implements OnInit {

  tableList: VendorInfo[] = [];
  currentPage = 1;
  limit = this.appService.limit;
  nzPageSizeOptions = this.appService.nzPageSizeOptions;
  count: any = 0;
  accountParamsobj: any = {
    limit: this.limit,
    pageNumber: this.currentPage
  };
  isTableLoading = false;

  expandSet = new Set<number>();
  vendorData!: VendorInfo;
  view = -1;
  searchVendor = '';

  constructor(
    public appService: AppService,
    private nzMessage: NzMessageService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getTableList();
  }

  getTableList(): any {
    this.accountParamsobj.limit = this.limit;
    this.accountParamsobj.pageNumber = this.currentPage - 1;
    this.isTableLoading = true;
    const url = 'vendors/page/' + this.accountParamsobj.pageNumber + '/' + this.accountParamsobj.limit;
    try {
      this.appService.getMethod(url)
        .subscribe((resp: any) => {
          console.log(resp);
          this.tableList = resp.data;
          console.log('this.tableList: ', this.tableList);
          this.count = resp.count;
          this.isTableLoading = false;
        },
          (resp: any) => {
            console.log('err', resp);
            this.isTableLoading = false;
            this.nzMessage.error(resp.name, resp.statusText);
          });
    } catch (e) {
    }
  }

  onNextPage(event: number): any {
    console.log('next page ', event);
    this.currentPage = event;
    this.getTableList();
  }

  onLimitChange(event: any): any {
    this.limit = event;
    this.currentPage = 1;
    this.getTableList();
  }

  onExpandChange(index: number, checked: boolean): void {
    if (checked) {
      this.expandSet.clear();
      this.view = index;
      this.getInfo(this.tableList[index]._id);
    } else {
      this.view = -1;
    }
  }

  getInfo(id: any): any {
    let url = 'vendors/' + id;
    try {
      this.appService.getMethod(url)
        .subscribe((resp: any) => {
          console.log('admin ', resp);
          this.vendorData = resp;
        }, (err: any) => {
          console.log('err', err);
          this.nzMessage.error(err.name, err.statusText);
        });
    } catch (e) {
    }
  }

  confirm(data: any): any {
    const url = 'vendors/' + data._id + '/delete';
    try {
      this.appService.patchMethod(url)
        .subscribe((resp: any) => {
          console.log(resp);
          if (resp.status === 200) {
            this.nzMessage.success(resp.body.additional_info);
            this.getTableList();
          }
        },
          (resp: any) => {
            console.log('err', resp);
            this.nzMessage.error(resp.name, resp.statusText);
          });
    } catch (e) {
    }
  }

  acceptVendor(data: any): any {
    const url = 'vendors/' + data._id + '/accept';
    try {
      this.appService.getMethod(url)
        .subscribe((resp: any) => {
          console.log(resp);
          if (resp.status === 200) {
            this.nzMessage.success(resp.body.additional_info);
            this.getTableList();
          }
        },
          (resp: any) => {
            console.log('err', resp);
            this.nzMessage.error(resp.name, resp.statusText);
          });
    } catch (e) {
    }
  }

  cancel(): any {}

  searchVendorByPhone(event: any): any {
    console.log('event: ', this.searchVendor);
    if (this.searchVendor.length > 3) {
      const url = 'vendors/search';
      const param = { q: this.searchVendor };
      this.isTableLoading = true;
      try {
        this.appService.getMethod(url, param)
          .subscribe((resp: any) => {
            if (resp) {
              this.tableList = [];
              this.tableList.push(resp);
              this.count = this.tableList.length;
            }
            this.isTableLoading = false;
          },
            (resp: any) => {
              console.log('err', resp);
              this.nzMessage.error(resp.name, resp.statusText);
              this.isTableLoading = false;
            });
      } catch (e) {
      }
    } else if (this.searchVendor === '') {
      this.getTableList();
    }
  }
}
