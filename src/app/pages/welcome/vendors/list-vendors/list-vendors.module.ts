import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListVendorsRoutingModule } from './list-vendors-routing.module';
import { ListVendorsComponent } from './list-vendors.component';
import { AccountSharedModule } from 'src/app/accounts.shared.module';


@NgModule({
  declarations: [ListVendorsComponent],
  imports: [
    CommonModule,
    ListVendorsRoutingModule,
    AccountSharedModule
  ]
})
export class ListVendorsModule { }
