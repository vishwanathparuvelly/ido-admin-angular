import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-create-vendor',
  templateUrl: './create-vendor.component.html',
  styleUrls: ['./create-vendor.component.css']
})
export class CreateVendorComponent implements OnInit {

  createForm!: FormGroup;
  times: any = [
    '0:00',
    '1:00',
    '2:00',
    '3:00',
    '4:00',
    '5:00',
    '6:00',
    '7:00',
    '8:00',
    '9:00',
    '10:00',
    '11:00',
    '12:00',
    '13:00',
    '14:00',
    '15:00',
    '16:00',
    '17:00',
    '18:00',
    '19:00',
    '20:00',
    '21:00',
    '22:00',
    '23:00',
    '24:00',
  ];

  constructor(
    private formBuilder: FormBuilder,
    private appService: AppService,
    private router: Router,
    private nzMessage: NzMessageService
  ) { }

  ngOnInit(): void {
    this.createForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
      phone_number: ['', [Validators.required, Validators.maxLength(10)]],
      company_name: ['', [Validators.required]],
      address_line1: ['', [Validators.required]],
      address_line2: '',
      city: ['', [Validators.required]],
      state: ['', [Validators.required]],
      landmark: ['', [Validators.required]],
      pincode: ['', [Validators.required]],
      GSTIN: ['', [Validators.required]],
      opening_time: ['', [Validators.required]],
      closing_time: ['', [Validators.required]]
    });
  }

  create(): any {
    const body = {
      name: this.createForm.controls.name.value,
      email: this.createForm.controls.email.value,
      password: this.createForm.controls.password.value,
      phone_number: this.createForm.controls.phone_number.value,
      company_name: this.createForm.controls.company_name.value,
      company_address: {
        address_line1: this.createForm.controls.address_line1.value,
        address_line2: this.createForm.controls.address_line2.value,
        city: this.createForm.controls.city.value,
        state: this.createForm.controls.state.value,
        landmark: this.createForm.controls.landmark.value,
        pincode: this.createForm.controls.pincode.value,
      },
      GSTIN: this.createForm.controls.GSTIN.value,
      opening_time: this.createForm.controls.opening_time.value,
      closing_time: this.createForm.controls.closing_time.value
    };
    try {
      this.appService.postMethod('vendors/new', body)
        .subscribe((resp: HttpResponse<any>) => {
          console.log('resp: ', resp);
          if (resp.status === 200) {
            this.nzMessage.success(resp.body.additional_info);
            this.router.navigate(['/welcome/vendors']);
          }
        },
          (resp: any) => {
            console.log(resp);
            this.nzMessage.error(resp.name, resp.statusText);
          });
    } catch (e) {
    }
  }

  cancel(): void {
    this.router.navigate(['/welcome/vendors']);
  }

  updatePwd(): any {
    const body = { password: this.createForm.controls.password.value };
    const url = 'admins/' + this.appService.dataObject.info._id + '/update_password';
    try {
      this.appService.patchMethod(url, JSON.stringify(body))
        .subscribe((resp: any) => {
          console.log(resp);
          if (resp.status === 200) {
            this.nzMessage.success(resp.body.additional_info);
            this.cancel();
          }
        },
          (resp: any) => {
            console.log('err', resp);
            this.nzMessage.error(resp.name, resp.statusText);
          });
    } catch (e) {
    }
  }

}
