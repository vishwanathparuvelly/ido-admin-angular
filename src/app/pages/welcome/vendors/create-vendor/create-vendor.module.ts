import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateVendorRoutingModule } from './create-vendor-routing.module';
import { CreateVendorComponent } from './create-vendor.component';
import { AccountSharedModule } from 'src/app/accounts.shared.module';


@NgModule({
  declarations: [CreateVendorComponent],
  imports: [
    CommonModule,
    CreateVendorRoutingModule,
    AccountSharedModule
  ]
})
export class CreateVendorModule { }
