import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AppService } from 'src/app/app.service';

export interface VendorInfo {
  'created_on': string;
  '_id': string;
  'email': string;
  'phone_number': string;
  'name': string;
}

@Component({
  selector: 'app-buyers',
  templateUrl: './buyers.component.html',
  styleUrls: ['./buyers.component.css']
})
export class BuyersComponent implements OnInit {

  tableList: VendorInfo[] = [];
  currentPage = 1;
  limit = this.appService.limit;
  nzPageSizeOptions = this.appService.nzPageSizeOptions;
  count: any = 0;
  accountParamsobj: any = {
    limit: this.limit,
    pageNumber: this.currentPage
  };
  isTableLoading = false;

  expandSet = new Set<number>();
  vendorData!: VendorInfo;
  view = -1;
  searchVendor = '';

  constructor(
    public appService: AppService,
    private nzMessage: NzMessageService
  ) { }

  ngOnInit(): void {
    this.getTableList();
  }

  getTableList(): any {
    this.accountParamsobj.limit = this.limit;
    this.accountParamsobj.pageNumber = this.currentPage - 1;
    this.isTableLoading = true;
    const url = 'buyers/page/' + this.accountParamsobj.pageNumber + '/' + this.accountParamsobj.limit;
    try {
      this.appService.getMethod(url)
        .subscribe((resp: any) => {
          console.log(resp);
          this.tableList = resp.data;
          console.log('this.tableList: ', this.tableList);
          this.count = resp.count;
          this.isTableLoading = false;
        },
          (resp: any) => {
            console.log('err', resp);
            this.isTableLoading = false;
            this.nzMessage.error(resp.name, resp.statusText);
          });
    } catch (e) {
    }
  }

  onNextPage(event: number): any {
    console.log('next page ', event);
    this.currentPage = event;
    this.getTableList();
  }

  onLimitChange(event: any): any {
    this.limit = event;
    this.currentPage = 1;
    this.getTableList();
  }

  onExpandChange(index: number, checked: boolean): void {
    if (checked) {
      this.expandSet.clear();
      this.view = index;
      this.getInfo(this.tableList[index]._id);
    } else {
      this.view = -1;
    }
  }

  getInfo(id: any): any {
    let url = 'buyers/' + id;
    try {
      this.appService.getMethod(url)
        .subscribe((resp: any) => {
          console.log('admin ', resp);
          this.vendorData = resp;
        }, (err: any) => {
          console.log('err', err);
          this.nzMessage.error(err.name, err.statusText);
        });
    } catch (e) {
    }
  }

  confirm(data: any): any {
    const url = 'buyers/' + data._id + '/delete';
    try {
      this.appService.deleteMethod(url)
        .subscribe((resp: any) => {
          console.log(resp);
          if (resp.status === 200) {
            this.nzMessage.success(resp.body.additional_info);
            this.currentPage=1
            this.getTableList();
          }
        },
          (resp: any) => {
            console.log('err', resp);
            this.nzMessage.error(resp.name, resp.statusText);
          });
    } catch (e) {
    }
  }

  cancel(): any {}

  searchVendorByPhone(event: any): any {
    console.log('event: ', this.searchVendor);
    if (this.searchVendor.length > 3) {
      const url = 'buyers/search';
      const param = { q: this.searchVendor };
      this.isTableLoading = true;
      try {
        this.appService.getMethod(url, param)
          .subscribe((resp: any) => {
            if (resp) {
              this.tableList = [];
              this.tableList.push(resp);
              this.count = this.tableList.length;
            }
            this.isTableLoading = false;
          },
            (resp: any) => {
              console.log('err', resp);
              this.nzMessage.error(resp.name, resp.statusText);
              this.isTableLoading = false;
            });
      } catch (e) {
      }
    } else if (this.searchVendor === '') {
      this.getTableList();
    }
  }

}
