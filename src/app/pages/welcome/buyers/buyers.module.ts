import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BuyersRoutingModule } from './buyers-routing.module';
import { BuyersComponent } from '../buyers/buyers.component';
import { AccountSharedModule } from 'src/app/accounts.shared.module';


@NgModule({
  declarations: [BuyersComponent],
  imports: [
    CommonModule,
    BuyersRoutingModule,
    AccountSharedModule
  ]
})
export class BuyersModule { }
