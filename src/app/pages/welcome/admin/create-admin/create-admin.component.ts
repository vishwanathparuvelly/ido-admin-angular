import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-create-admin',
  templateUrl: './create-admin.component.html',
  styleUrls: ['./create-admin.component.css']
})
export class CreateAdminComponent implements OnInit {

  createAdminForm!: FormGroup;
  type = true;

  constructor(
    private formBuilder: FormBuilder,
    private appService: AppService,
    private notification: NzNotificationService,
    private router: Router,
    private nzMessage: NzMessageService
  ) { }

  ngOnInit(): void {
    this.createAdminForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
      manage_admins: true,
      manage_buyers: true,
      manage_vendors: true,
      manage_categories: true,
      manage_products: true,
      manage_orders: true,
      manage_coupons: true,
      manage_banners: true,
      manage_wholesale_categories: true,
      manage_wholesale_products: true,
      manage_wholesale_orders: true,
      manage_wholesale_coupons: true,
      manage_wholesale_banners: true
    });
    this.type = this.appService.dataObject ? this.appService.dataObject.new : true;
    if (this.type === false) {
      console.log('this.appService.dataObject.info: ', this.appService.dataObject.info);
      this.createAdminForm.controls.name.setValue(this.appService.dataObject.info.name);
      this.createAdminForm.controls.email.setValue(this.appService.dataObject.info.email);
      this.createAdminForm.controls.name.disable();
      this.createAdminForm.controls.email.disable();
    }
  }

  createAdmin(): any {
    const body = {
      name: this.createAdminForm.controls.name.value,
      email: this.createAdminForm.controls.email.value,
      password: this.createAdminForm.controls.password.value,
      permissions: {
        manage_admins: this.createAdminForm.controls.manage_admins.value.toString(),
        manage_buyers: this.createAdminForm.controls.manage_buyers.value.toString(),
        manage_vendors: this.createAdminForm.controls.manage_vendors.value.toString(),
        manage_categories: this.createAdminForm.controls.manage_categories.value.toString(),
        manage_products: this.createAdminForm.controls.manage_products.value.toString(),
        manage_orders: this.createAdminForm.controls.manage_orders.value.toString(),
        manage_coupons: this.createAdminForm.controls.manage_coupons.value.toString(),
        manage_banners: this.createAdminForm.controls.manage_banners.value.toString(),
        manage_wholesale_categories: this.createAdminForm.controls.manage_wholesale_categories.value.toString(),
        manage_wholesale_products: this.createAdminForm.controls.manage_wholesale_products.value.toString(),
        manage_wholesale_orders: this.createAdminForm.controls.manage_wholesale_orders.value.toString(),
        manage_wholesale_coupons: this.createAdminForm.controls.manage_wholesale_coupons.value.toString(),
        manage_wholesale_banners: this.createAdminForm.controls.manage_wholesale_banners.value.toString()
      }
    };
    try {
      this.appService.postMethod('admins/new', body)
        .subscribe((resp: HttpResponse<any>) => {
          console.log('resp: ', resp);
          if (resp.status === 200) {
            this.nzMessage.success(resp.body.additional_info);
            this.router.navigate(['/welcome/admin']);
          }
        },
          (resp: any) => {
            console.log(resp);
            this.nzMessage.error(resp.name, resp.statusText);
          });
    } catch (e) {
    }
  }

  cancel(): void {
    this.router.navigate(['/welcome/admin']);
  }

  updatePwd(): any {
    const body = { password: this.createAdminForm.controls.password.value };
    const url = 'admins/' + this.appService.dataObject.info._id + '/update_password';
    try {
      this.appService.patchMethod(url, JSON.stringify(body))
        .subscribe((resp: any) => {
          console.log(resp);
          if (resp.status === 200) {
            this.nzMessage.success(resp.body.additional_info);
            this.cancel();
          }
        },
          (resp: any) => {
            console.log('err', resp);
            this.nzMessage.error(resp.name, resp.statusText);
          });
    } catch (e) {
    }
  }
}
