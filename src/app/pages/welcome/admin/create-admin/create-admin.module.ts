import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateAdminRoutingModule } from './create-admin-routing.module';
import { CreateAdminComponent } from './create-admin.component';

import { AccountSharedModule } from 'src/app/accounts.shared.module';

@NgModule({
  declarations: [
    CreateAdminComponent
  ],
  imports: [
    CommonModule,
    CreateAdminRoutingModule,
    AccountSharedModule
  ]
})
export class CreateAdminModule { }
