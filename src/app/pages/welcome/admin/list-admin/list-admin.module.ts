import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListAdminRoutingModule } from './list-admin-routing.module';
import { ListAdminComponent } from './list-admin.component';
import { AccountSharedModule } from 'src/app/accounts.shared.module';


@NgModule({
  declarations: [
    ListAdminComponent
  ],
  imports: [
    CommonModule,
    ListAdminRoutingModule,
    AccountSharedModule
  ],
  providers: []
})
export class ListAdminModule { }
