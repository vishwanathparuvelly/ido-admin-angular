import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AppService } from 'src/app/app.service';

export interface AdminData {
  '_id': string;
  'name': string;
  'email': string;
}

export interface Permissions {
  'manage_admins': any;
  'manage_buyers': any;
  'manage_vendors': any;
  'manage_categories': any;
  'manage_products': any;
  'manage_orders': any;
  'manage_coupons': any;
  'manage_banners': any;
  'manage_wholesale_categories': any;
  'manage_wholesale_products': any;
  'manage_wholesale_orders': any;
  'manage_wholesale_coupons': any;
  'manage_wholesale_banners': any;
}

@Component({
  selector: 'app-list-admin',
  templateUrl: './list-admin.component.html',
  styleUrls: ['./list-admin.component.css']
})
export class ListAdminComponent implements OnInit {

  tableList: AdminData[] = [];
  currentPage = 1;
  limit = this.appService.limit;
  nzPageSizeOptions = this.appService.nzPageSizeOptions;
  count: any = 0;
  accountParamsobj: any = {
    limit: this.limit,
    pageNumber: this.currentPage
  };
  isTableLoading = false;

  expandSet = new Set<number>();
  AdminData: any;
  Permissions!: Permissions;
  view = -1;

  constructor(
    public appService: AppService,
    private nzMessage: NzMessageService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getTableList();
  }

  getTableList(): any {
    this.accountParamsobj.limit = this.limit;
    this.accountParamsobj.pageNumber = this.currentPage - 1;
    this.isTableLoading = true;
    let url = 'admins/page/' + this.accountParamsobj.pageNumber + '/' + this.accountParamsobj.limit;
    try {
      this.appService.getMethod(url)
        .subscribe((resp: any) => {
          console.log(resp);
          this.tableList = resp.data;
          console.log('this.tableList: ', this.tableList);
          this.count = resp.count;
          this.isTableLoading = false;
        },
          (resp: any) => {
            console.log('err', resp);
            this.isTableLoading = false;
            this.nzMessage.error(resp.name, resp.statusText);
          });
    } catch (e) {
    }
  }

  onNextPage(event: number): any {
    console.log(event);
    this.currentPage = event;
    this.getTableList();
  }

  onLimitChange(event: any): any {
    this.limit = event;
    this.currentPage = 1;
    this.getTableList();
  }

  confirm(data: any): any {
    let url = 'admins/' + data._id + '/delete';
    try {
      this.appService.patchMethod(url)
        .subscribe((resp: any) => {
          console.log(resp);
          if (resp.status === 200) {
            this.nzMessage.success(resp.body.additional_info);
            this.getTableList();
          }
        },
          (resp: any) => {
            console.log('err', resp);
            this.nzMessage.error(resp.name, resp.statusText);
          });
    } catch (e) {
    }
  }

  cancel(): any {}

  update(data: any): any {
    this.appService.dataObject = { new: false, info: data };
    this.router.navigate(['welcome', 'admin', 'create-admin']);
  }

  onExpandChange(index: number, checked: boolean): void {
    if (checked) {
      this.view = index;
      this.getAdminInfo(this.tableList[index]._id);
    } else {
      this.view = -1;
    }
  }

  getAdminInfo(id: any): any {
    let url = 'admins/' + id;
    try {
      this.appService.getMethod(url)
        .subscribe((resp: any) => {
          console.log('admin ', resp);
          this.AdminData = resp;
          this.Permissions = resp.permissions;
        }, (err: any) => {
          console.log('err', err);
          this.nzMessage.error(err.name, err.statusText);
        });
    } catch (e) {
    }
  }

  updatePermissions(id: any): any {
    const body = { permissions: this.Permissions };
    const url = 'admins/' + id + '/update_permissions';
    this.Permissions.manage_admins = this.Permissions.manage_admins.toString();
    this.Permissions.manage_banners = this.Permissions.manage_banners.toString();
    this.Permissions.manage_buyers = this.Permissions.manage_buyers.toString();
    this.Permissions.manage_categories = this.Permissions.manage_categories.toString();
    this.Permissions.manage_coupons = this.Permissions.manage_coupons.toString();
    this.Permissions.manage_orders = this.Permissions.manage_orders.toString();
    this.Permissions.manage_products = this.Permissions.manage_products.toString();
    this.Permissions.manage_vendors = this.Permissions.manage_vendors.toString();
    this.Permissions.manage_wholesale_banners = this.Permissions.manage_wholesale_banners.toString();
    this.Permissions.manage_wholesale_categories = this.Permissions.manage_wholesale_categories.toString();
    this.Permissions.manage_wholesale_coupons = this.Permissions.manage_wholesale_coupons.toString();
    this.Permissions.manage_wholesale_orders = this.Permissions.manage_wholesale_orders.toString();
    this.Permissions.manage_wholesale_products = this.Permissions.manage_wholesale_products.toString();
    try {
      this.appService.patchMethod(url, JSON.stringify(body))
        .subscribe((resp: any) => {
          console.log(resp);
          if (resp.status === 200) {
            this.nzMessage.success(resp.body.additional_info);
            this.getAdminInfo(id);
          }
        },
          (resp: any) => {
            console.log('err', resp);
            this.nzMessage.error(resp.name, resp.statusText);
          });
    } catch (e) {
    }
  }
}
