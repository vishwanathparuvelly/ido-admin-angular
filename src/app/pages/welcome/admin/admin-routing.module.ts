import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';

const routes: Routes = [
  {
    path: '', component: AdminComponent,
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'prefix' },
      {
        path: 'create-admin', loadChildren: () => import('./create-admin/create-admin.module').then((m) => m.CreateAdminModule)
      },
      {
        path: 'list', loadChildren: () => import('./list-admin/list-admin.module').then((m) => m.ListAdminModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
