import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  currentUrl = '';
  isNavItemNumber = 2;

  constructor(
    private router: Router,
    private appService: AppService
  ) {
    router.events.forEach((event) => {
      if (event instanceof NavigationEnd) {
        console.log(event.url);
        const currentUrl = event.url;
        const currentUrlArray = currentUrl.split('/');
        this.currentUrl = currentUrlArray[3];
        console.log('this.currentUrl: ', this.currentUrl);
        if (this.currentUrl === 'create-admin') {
          this.isNavItemNumber = 1;
        } else {
          this.isNavItemNumber = 2;
        }
      }
      // NavigationEnd
      // NavigationCancel
      // NavigationError
      // RoutesRecognized
    });
  }

  ngOnInit(): void {
  }

  createAdmin(): any {
    this.appService.dataObject = { new: true, info: {} };
    this.router.navigate(['welcome', 'admin', 'create-admin']);
  }

}
