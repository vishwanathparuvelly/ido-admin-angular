import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WholesaleProductsRoutingModule } from './wholesale-products-routing.module';
import { WholesaleProductsComponent } from './wholesale-products.component';
import { AccountSharedModule } from 'src/app/accounts.shared.module';


@NgModule({
  declarations: [
    WholesaleProductsComponent
  ],
  imports: [
    CommonModule,
    WholesaleProductsRoutingModule,
    AccountSharedModule
  ]
})
export class WholesaleProductsModule { }
