import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WholesaleProductsComponent } from './wholesale-products.component';

const routes: Routes = [
  {
    path: '', component: WholesaleProductsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WholesaleProductsRoutingModule { }
