import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AppService } from 'src/app/app.service';

export interface ProductInfo {
  'discount_percentage': string;
  '_id': string;
  'available': string;
  'images': [];
  'name': string;
  'price': number;
  'description': string;
  'created_on': string;
}

export interface Categories {
  'parent': string;
  '_id': string;
  'name': string;
}

@Component({
  selector: 'app-wholesale-products',
  templateUrl: './wholesale-products.component.html',
  styleUrls: ['./wholesale-products.component.css']
})
export class WholesaleProductsComponent implements OnInit {

  tableList: ProductInfo[] = [];
  currentPage = 1;
  limit = this.appService.limit;
  nzPageSizeOptions = this.appService.nzPageSizeOptions;
  count: any = 0;
  accountParamsobj: any = {
    limit: this.limit,
    pageNumber: this.currentPage
  };
  isTableLoading = false;

  expandSet = new Set<number>();
  vendorData!: ProductInfo;
  view = -1;
  searchVendor = '';
  isVisible = false;
  categoryName = '';
  actionType = 'N';
  updateId: any;
  isVisibleUpdate = false;
  createProductForm!: FormGroup;
  CategoriesList: Categories[] = [];
  byCategory = 'ALL';

  constructor(
    public appService: AppService,
    private nzMessage: NzMessageService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.createProductForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      description: ['', [Validators.required]],
      price: ['', [Validators.required]],
      discount_percentage: ['', [Validators.required]],
      category_id: ['', [Validators.required]]
    });
    this.getTableList();
    this.getCategories();
  }

  byCategoryProducts(): any {
    this.currentPage = 1;
    this.getTableList();
  }

  getTableList(): any {
    this.accountParamsobj.limit = this.limit;
    this.accountParamsobj.pageNumber = this.currentPage - 1;
    this.isTableLoading = true;
    let url = '';
    if (this.byCategory === 'ALL') {
      url = 'wholesale_products/page/' + this.accountParamsobj.pageNumber + '/' + this.accountParamsobj.limit;
    } else if (this.byCategory !== 'ALL') {
      url = 'wholesale_products/category/' + this.byCategory + '/page/' + this.accountParamsobj.pageNumber + '/' + this.accountParamsobj.limit;
    }
    try {
      this.appService.getMethod(url)
        .subscribe((resp: any) => {
          console.log(resp);
          this.tableList = resp.data;
          console.log('this.tableList: ', this.tableList);
          this.count = resp.count;
          this.isTableLoading = false;
        },
          (resp: any) => {
            console.log('err', resp);
            this.isTableLoading = false;
            this.nzMessage.error(resp.name, resp.statusText);
          });
    } catch (e) {
    }
  }

  onNextPage(event: number): any {
    console.log('next page ', event);
    this.view = -1;
    this.currentPage = event;
    this.getTableList();
  }

  onLimitChange(event: any): any {
    this.limit = event;
    this.currentPage = 1;
    this.getTableList();
  }

  onExpandChange(index: number, checked: boolean): void {
    if (checked) {
      this.expandSet.clear();
      this.view = index;
      this.getInfo(this.tableList[index]._id);
    } else {
      this.view = -1;
    }
  }

  getInfo(id: any): any {
    const url = 'wholesale_products/' + id;
    try {
      this.appService.getMethod(url)
        .subscribe((resp: any) => {
          console.log('admin ', resp);
          this.vendorData = resp;
        }, (err: any) => {
          console.log('err', err);
          this.nzMessage.error(err.name, err.statusText);
        });
    } catch (e) {
    }
  }

  confirm(data: any): any {
    const url = 'wholesale_products/' + data._id + '/delete';
    try {
      this.appService.patchMethod(url)
        .subscribe((resp: any) => {
          console.log(resp);
          if (resp.status === 200) {
            this.nzMessage.success(resp.body.additional_info);
            this.getTableList();
          }
        },
          (resp: any) => {
            console.log('err', resp);
            this.nzMessage.error(resp.name, resp.statusText);
          });
    } catch (e) {
    }
  }

  cancel(): any { }

  create(action: string, data?: any): any {
    this.actionType = action;
    if (data) {
      this.categoryName = data.name;
      this.updateId = data._id;
      this.isVisibleUpdate = true;
      this.createProductForm.controls.name.setValue(data.name);
      this.createProductForm.controls.description.setValue(data.description);
      this.createProductForm.controls.price.setValue(data.price);
      this.createProductForm.controls.discount_percentage.setValue(data.discount_percentage);
    } else {
      this.isVisible = true;
    }
  }

  updateProduct(): any {
    const url = 'wholesale_products/' + this.updateId + '/update';
    try {
      this.appService.patchMethod(url, {
        name: this.createProductForm.controls.name.value,
        description: this.createProductForm.controls.description.value,
        price: this.createProductForm.controls.price.value.toString(),
        discount_percentage: this.createProductForm.controls.discount_percentage.value.toString()
      })
        .subscribe((resp: any) => {
          console.log(resp);
          if (resp.status === 200) {
            this.isVisibleUpdate = false;
            this.view = -1;
            this.nzMessage.success(resp.body.additional_info);
            this.getTableList();
          }
        },
          (resp: any) => {
            console.log('err', resp);
            this.nzMessage.error(resp.name, resp.statusText);
          });
    } catch (e) {
    }
  }

  handleOk(): void {
    const url = 'wholesale_products/new';
    const body = {
      name: this.createProductForm.controls.name.value,
      description: this.createProductForm.controls.description.value,
      price: this.createProductForm.controls.price.value.toString(),
      discount_percentage: this.createProductForm.controls.discount_percentage.value.toString(),
      category_id: this.createProductForm.controls.category_id.value
    };
    try {
      this.appService.postMethod(url, body)
        .subscribe((resp: any) => {
          console.log(resp);
          this.isVisible = false;
          if (resp.status === 200) {
            this.categoryName = '';
            this.nzMessage.success(resp.body.additional_info);
            this.getTableList();
          }
        },
          (err: any) => {
            console.log('err', err);
            this.nzMessage.error(err.name, err.statusText);
          });
    } catch (e) {
    }
  }

  handleCancel(): void {
    this.isVisible = false;
  }

  handleCancelUpdate(): void {
    this.isVisibleUpdate = false;
  }

  getCategories(): any {
    const url = 'wholesale_categories';
    try {
      this.appService.getMethod(url)
        .subscribe((resp: any) => {
          console.log(resp);
          this.CategoriesList = resp;
        },
          (resp: any) => {
            console.log('err', resp);
            this.nzMessage.error(resp.name, resp.statusText);
          });
    } catch (e) {
    }
  }

}
