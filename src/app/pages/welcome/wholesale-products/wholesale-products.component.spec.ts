import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WholesaleProductsComponent } from './wholesale-products.component';

describe('WholesaleProductsComponent', () => {
  let component: WholesaleProductsComponent;
  let fixture: ComponentFixture<WholesaleProductsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WholesaleProductsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WholesaleProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
