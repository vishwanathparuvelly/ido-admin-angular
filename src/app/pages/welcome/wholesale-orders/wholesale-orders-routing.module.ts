import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WholesaleOrdersComponent } from './wholesale-orders.component';

const routes: Routes = [
  {
    path: '', component: WholesaleOrdersComponent, children: [
      { path: '', redirectTo: 'list', pathMatch: 'prefix' },
      {
        path: 'list',
        loadChildren: () =>
          import('./list/list.module').then((m) => m.ListModule),
      },
      {
        path: 'view/:id',
        loadChildren: () =>
          import('./view/view.module').then((m) => m.ViewModule),
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WholesaleOrdersRoutingModule { }
