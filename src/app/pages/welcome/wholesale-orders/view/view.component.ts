import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AppService } from 'src/app/app.service';
import * as moment from 'moment';

export interface ProductInfo {
  'discount_percentage': string;
  '_id': string;
  'available': string;
  'images': [];
  'name': string;
  'price': number;
  'description': string;
  'created_on': string;
}

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  selectedTab = 0;
  orderId: any;
  orderInfo: any;
  buyerInfo: any;
  productInfo: ProductInfo[] = [];
  temp: ProductInfo[] = [];

  isTableLoading = false;
  view = -1;
  isVisible = false;
  tracking_number = '';
  courier = '';
  edd = '';
  actionName = '';

  constructor(
    public appService: AppService,
    private nzMessage: NzMessageService,
    private activeRouter: ActivatedRoute,
    private router: Router
  ) {
    this.activeRouter.params.subscribe(data => {
      this.orderId = data.id;
    });
  }

  ngOnInit(): void {
    this.getInfo(this.orderId, 'ORDER');
  }

  tabChange(event: number): any {
    this.selectedTab = event;
    if (this.selectedTab === 1) {
      // for (let pi = 0; pi < this.orderInfo.prducts.length; pi++) {
        this.getInfo(this.orderInfo.products[0].product, 'PRODUCT', 0);
      // }
    }
  }

  getInfo(id: any, type: string, ind?: any): any {
    let url = '';
    if (type === 'ORDER') {
      url = 'wholesale_orders/' + id;
    }
    if (type === 'BUYER') {
      url = 'buyers/' + id;
    }
    if (type === 'PRODUCT') {
      url = 'products/' + id;
    }
    try {
      this.appService.getMethod(url)
        .subscribe((resp: any) => {
          console.log('order Info ', resp);
          if (type === 'ORDER') {
            this.orderInfo = resp;
            console.log('this.orderInfo: ', this.orderInfo);
            setTimeout(() => {
              console.log('this.orderInfo buyer: ', this.orderInfo.buyer);
              this.getInfo(this.orderInfo.buyer, 'BUYER')
            }, 1000);
          }
          if (type === 'BUYER') {
            this.buyerInfo = resp;
            console.log('this.buyerInfo: ', this.buyerInfo);
          }
          if (type === 'PRODUCT') {
            this.temp.push(resp);
            console.log('ind: ', ind);
            let i = ind + 1;
            if (i < this.orderInfo.products.length) {
              this.getInfo(this.orderInfo.products[i].product, 'PRODUCT', i);
            } else {
              this.productInfo = this.temp;
            }
            console.log('this.productInfo: ', this.productInfo);
          }
        }, (err: any) => {
          console.log('err', err);
          this.nzMessage.error(err.name, err.statusText);
        });
    } catch (e) {
    }
  }

  updateStatus(type: string, id: string): any {
    const url = 'wholesale_orders/' + id + '/' + type;
    try {
      this.appService.getMethod(url)
        .subscribe((resp: any) => {
          if (resp.status === 200) {
            this.nzMessage.success(resp.body.additional_info);
            this.ngOnInit();
          }
        }, (err: any) => {
          console.log('err', err);
          this.nzMessage.error(err.name, err.statusText);
        });
    } catch (e) {
    }
  }

  cancelupdate(): any {}

  handleCancel(): void {
    this.isVisible = false;
  }

  shipOrder(type: string, id: string): any {
    this.isVisible = true;
    this.actionName = 'Ship Order';
    this.orderId = id;
  }

  onChange(event: Date): any {
    if (event !== null) {
      this.edd = moment(event).format('l');
    }
  }

  handleOk(): any {
    const url = 'wholesale_orders/' + this.orderId + '/ship';
    const body = {
      courier: this.courier,
      tracking_number: this.tracking_number,
      edd: this.edd
    };
    try {
      this.appService.postMethod(url, body)
        .subscribe((resp: any) => {
          this.isVisible = false;
          if (resp.status === 200) {
            this.nzMessage.success(resp.body.additional_info);
            this.ngOnInit();
          }
        }, (err: any) => {
          console.log('err', err);
          this.isVisible = false;
          this.nzMessage.error(err.name, err.statusText);
        });
    } catch (e) {
    }
  }

}
