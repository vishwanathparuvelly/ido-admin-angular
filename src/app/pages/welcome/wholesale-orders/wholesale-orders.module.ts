import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WholesaleOrdersRoutingModule } from './wholesale-orders-routing.module';
import { WholesaleOrdersComponent } from './wholesale-orders.component';

@NgModule({
  declarations: [
    WholesaleOrdersComponent
  ],
  imports: [
    CommonModule,
    WholesaleOrdersRoutingModule
  ]
})
export class WholesaleOrdersModule { }
