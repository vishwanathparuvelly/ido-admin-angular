import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CouponsRoutingModule } from './coupons-routing.module';
import { CouponsComponent } from '../coupons/coupons.component';
import { AccountSharedModule } from 'src/app/accounts.shared.module';


@NgModule({
  declarations: [CouponsComponent],
  imports: [
    CommonModule,
    CouponsRoutingModule,
    AccountSharedModule
  ]
})
export class CouponsModule { }
