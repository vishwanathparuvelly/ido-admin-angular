import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AppService } from 'src/app/app.service';

export interface Coupons {
  'coupon_code': string;
  '_id': string;
  'type': string;
  'value': number;
}

@Component({
  selector: 'app-coupons',
  templateUrl: './coupons.component.html',
  styleUrls: ['./coupons.component.css']
})
export class CouponsComponent implements OnInit {

  selectedTab = 0;
  isTableLoading = false;
  selectedId = '';
  isVisible = false;
  categoryType = 1;
  couponCode = '';
  type = '';
  value = '';
  actionType = 'N';
  updateId: any;
  isVisibleUpdate = false;
  tableList: Coupons[] = [];
  currentPage = 1;
  limit = this.appService.limit;
  nzPageSizeOptions = this.appService.nzPageSizeOptions;
  count: any = 0;
  accountParamsobj: any = {
    limit: this.limit,
    pageNumber: this.currentPage
  };
  view = -1;

  constructor(
    public appService: AppService,
    private nzMessage: NzMessageService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.getTableList();
  }

  getTableList(): any {
    this.accountParamsobj.limit = this.limit;
    this.accountParamsobj.pageNumber = this.currentPage - 1;
    this.isTableLoading = true;
    let url = '';
    if (this.selectedTab === 0) {
      url = 'coupons/page/' + this.accountParamsobj.pageNumber + '/' + this.accountParamsobj.limit;
    } else if (this.selectedTab === 1) {
      url = 'wholesale_coupons/page/' + this.accountParamsobj.pageNumber + '/' + this.accountParamsobj.limit;
    }
    try {
      this.appService.getMethod(url)
        .subscribe((resp: any) => {
          console.log(resp);
          this.tableList = resp.data;
          console.log('this.tableList: ', this.tableList);
          this.count = resp.count;
          this.isTableLoading = false;
        },
          (resp: any) => {
            console.log('err', resp);
            this.isTableLoading = false;
            this.nzMessage.error(resp.name, resp.statusText);
          });
    } catch (e) {
    }
  }

  onNextPage(event: number): any {
    console.log('next page ', event);
    this.view = -1;
    this.currentPage = event;
    this.getTableList();
  }

  onLimitChange(event: any): any {
    this.limit = event;
    this.currentPage = 1;
    this.getTableList();
  }

  onExpandChange(index: number, checked: boolean): void {
    if (checked) {
      this.view = index;
      // this.getInfo(this.tableList[index]._id);
    } else {
      this.view = -1;
    }
  }

  cancel(): any {}

  confirmCategory(data: any, type: number): any {
    this.categoryType = type;
    let url = '';
    if (this.selectedTab === 0) {
      url = 'coupons/' + data._id + '/delete';
    } else if (this.selectedTab === 1) {
      url = 'wholesale_coupons/' + data._id + '/delete';
    }
    try {
      this.appService.patchMethod(url)
        .subscribe((resp: any) => {
          console.log(resp);
          if (resp.status === 200) {
            this.nzMessage.success(resp.body.additional_info);
            this.getTableList();
          }
        },
          (resp: any) => {
            console.log('err', resp);
            this.nzMessage.error(resp.name, resp.statusText);
          });
    } catch (e) {
    }
  }

  tabChange(event: number): any {
    this.selectedTab = event;
    this.selectedId = '';
    this.getTableList();
  }

  create(): any {
    // this.categoryType = type;
    // this.actionType = action;
    // if (data) {
    //   this.categoryName = data.name;
    //   this.updateId = data._id;
    //   this.isVisibleUpdate = true;
    // } else {
    // }
    this.isVisible = true;
  }

  handleOk(): void {
    let url = '';
    let body;
    if (this.selectedTab === 0) {
      url = 'coupons/new';
    } else if (this.selectedTab === 1) {
      url = 'wholesale_coupons/new';
    }
    body = { coupon_code: this.couponCode, type: this.type, value: this.value };
    try {
      this.appService.postMethod(url, body)
        .subscribe((resp: any) => {
          console.log(resp);
          this.isVisible = false;
          if (resp.status === 200) {
            this.couponCode = '';
            this.type = '';
            this.value = '';
            this.nzMessage.success(resp.body.additional_info);
            this.getTableList();
          }
        },
          (err: any) => {
            console.log('err', err);
            this.nzMessage.error(err.name, err.statusText);
          });
    } catch (e) {
    }
  }

  handleCancel(): void {
    console.log('Button cancel clicked!');
    this.isVisible = false;
  }

}
