import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WholesaleCategoriesRoutingModule } from './wholesale-categories-routing.module';
import { WholesaleCategoriesComponent } from './wholesale-categories.component';
import { AccountSharedModule } from 'src/app/accounts.shared.module';


@NgModule({
  declarations: [
    WholesaleCategoriesComponent
  ],
  imports: [
    CommonModule,
    WholesaleCategoriesRoutingModule,
    AccountSharedModule
  ]
})
export class WholesaleCategoriesModule { }
