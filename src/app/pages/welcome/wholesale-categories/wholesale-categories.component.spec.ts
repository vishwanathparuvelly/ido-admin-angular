import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WholesaleCategoriesComponent } from './wholesale-categories.component';

describe('WholesaleCategoriesComponent', () => {
  let component: WholesaleCategoriesComponent;
  let fixture: ComponentFixture<WholesaleCategoriesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WholesaleCategoriesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WholesaleCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
