import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderComponent } from './order.component';

const routes: Routes = [
  {
    path: '', component: OrderComponent, children: [
      { path: '', redirectTo: 'list', pathMatch: 'prefix' },
      {
        path: 'list', loadChildren: () => import('../orders.module').then(m => m.OrdersModule)
      },
      {
        path: 'view/:id', loadChildren: () => import('../view-order/view-order.module').then(m => m.ViewOrderModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderRoutingModule { }
