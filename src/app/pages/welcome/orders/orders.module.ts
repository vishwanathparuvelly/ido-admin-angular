import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrdersRoutingModule } from './orders-routing.module';
import { OrdersComponent } from '../orders/orders.component';
import { AccountSharedModule } from 'src/app/accounts.shared.module';

@NgModule({
  declarations: [OrdersComponent],
  imports: [
    CommonModule,
    OrdersRoutingModule,
    AccountSharedModule
  ]
})
export class OrdersModule { }
