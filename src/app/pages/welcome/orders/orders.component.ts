import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AppService } from 'src/app/app.service';

export interface BuyerInfo {
  'created_on': string;
  '_id': string;
  'email': string;
  'phone_number': string;
  'name': string;
}

export interface OrderInfo {
  'created_on': string;
  '_id': string;
  'status': string;
  'buyer': string;
  'address': string;
  'products': [];
  'bill_amount': number;
  'discount_coupon': string;
}

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

  BuyersList: BuyerInfo[] = [];
  tableList: OrderInfo[] = [];
  currentPage = 1;
  limit = this.appService.limit;
  nzPageSizeOptions = this.appService.nzPageSizeOptions;
  count: any = 0;
  accountParamsobj: any = {
    limit: this.limit,
    pageNumber: this.currentPage
  };
  isTableLoading = false;

  buyerName = '';
  view = -1;
  buyerId = '';
  orderId: any;

  constructor(
    public appService: AppService,
    private nzMessage: NzMessageService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getTableList(1);
    this.getOrders(1);
  }

  getTableList(type: number): any {
    let url = '';
    let param = { q: '' };
    if (type === 1) {
      url = 'buyers/page/0/5';
      try {
        this.appService.getMethod(url)
          .subscribe((resp: any) => {
            console.log(resp);
            this.BuyersList = resp.data;
            console.log('this.tableList: ', this.tableList);
            this.count = resp.count;
          },
            (resp: any) => {
              console.log('err', resp);
              this.nzMessage.error(resp.name, resp.statusText);
            });
      } catch (e) {
      }
    } else if (type === 2) {
      url = 'buyers/search';
      param = { q: this.buyerName };
      try {
        this.appService.getMethod(url, param)
          .subscribe((resp: any) => {
            console.log('resp ', resp);
            if (resp) {
              this.BuyersList = [];
              this.BuyersList.push(resp);
              this.buyerId = resp._id;
              // this.count = this.BuyersList.length;
            }
            console.log('this.tableList: ', this.BuyersList);
          },
            (err: any) => {
              console.log('err', err);
              this.nzMessage.error(err.name, err.statusText);
            });
      } catch (e) {
      }
    }
  }

  onSearchFilterControl(event: any, type: number): any {
    console.log('event: ', event);
    this.buyerName = event;
    if (this.buyerName.length > 9) {
      this.getTableList(type);
    }
  }

  onSelectBuyer(event: any): any {
    console.log('buyerid ', this.buyerId);
    console.log('event ', event);
    this.buyerId = event;
    if (this.buyerId !== null) {
      this.getOrders(2);
    } else {
      this.getOrders(1);
    }
  }

  getOrders(type: number): any {
    let url = '';
    this.isTableLoading = true;
    this.accountParamsobj.limit = this.limit;
    this.accountParamsobj.pageNumber = this.currentPage - 1;
    if (type === 1) {
      url = 'orders/page/' + this.accountParamsobj.pageNumber + '/' + this.accountParamsobj.limit;
    } else if (type === 2) {
      url = 'orders/buyer/' + this.buyerId;
    }
    try {
      this.appService.getMethod(url)
        .subscribe((resp: any) => {
          console.log(resp);
          this.tableList = resp.data;
          console.log('this.tableList: ', this.tableList);
          this.count = resp.count;
          this.isTableLoading = false;
        },
          (resp: any) => {
            console.log('err', resp);
            this.isTableLoading = false;
            this.nzMessage.error(resp.name, resp.statusText);
          });
    } catch (e) {
    }
  }

  onNextPage(event: number): any {
    console.log('next page ', event);
    this.currentPage = event;
    this.getOrders(1);
  }

  onLimitChange(event: any): any {
    this.limit = event;
    this.currentPage = 1;
    this.getOrders(1);
  }

  onExpandChange(index: number): void {
      this.router.navigateByUrl('/welcome/order/view/' + this.tableList[index]._id);
  }

  confirm(data: any): any {
    const url = 'orders/' + data._id + '/delete';
    try {
      this.appService.patchMethod(url)
        .subscribe((resp: any) => {
          console.log(resp);
          if (resp.status === 200) {
            this.nzMessage.success(resp.body.additional_info);
            if (this.buyerId !== null) {
              this.getOrders(2);
            } else {
              this.getOrders(1);
            }
          }
        },
          (resp: any) => {
            console.log('err', resp);
            this.nzMessage.error(resp.name, resp.statusText);
          });
    } catch (e) {
    }
  }

  cancel(): any { }

  updateStatus(type: string, id: string): any {
    const url = 'orders/' + id + '/' + type;
    try {
      this.appService.getMethod(url)
        .subscribe((resp: any) => {
          if (resp.status === 200) {
            this.nzMessage.success(resp.body.additional_info);
            this.ngOnInit();
          }
        }, (err: any) => {
          console.log('err', err);
          this.nzMessage.error(err.name, err.statusText);
        });
    } catch (e) {
    }
  }

  cancelupdate(): any {}

}
