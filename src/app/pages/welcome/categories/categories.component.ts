import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AppService } from 'src/app/app.service';

export interface Categories {
  'parent': string;
  '_id': string;
  'name': string;
}

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  selectedId = '';
  CategoriesList: Categories[] = [];
  currentPage = 1;
  limit = this.appService.limit;
  nzPageSizeOptions = this.appService.nzPageSizeOptions;
  count: any = 0;
  accountParamsobj: any = {
    limit: this.limit,
    pageNumber: this.currentPage
  };
  isTableLoading = false;
  rootId = '';
  view = -1;
  SubCategories: Categories[] = [];
  isVisible = false;
  categoryType = 1;
  categoryName = '';
  actionType = 'N';
  updateId: any;
  isVisibleUpdate = false;

  constructor(
    public appService: AppService,
    private nzMessage: NzMessageService
  ) { }

  ngOnInit(): void {
    this.getTableList();
  }

  getTableList(): any {
    this.isTableLoading = true;
    const url = 'categories';
    try {
      this.appService.getMethod(url)
        .subscribe((resp: any) => {
          console.log(resp);
          this.CategoriesList = resp;
          this.count = resp.count;
          this.isTableLoading = false;
        },
          (resp: any) => {
            console.log('err', resp);
            this.isTableLoading = false;
            this.nzMessage.error(resp.name, resp.statusText);
          });
    } catch (e) {
    }
  }

  onNextPage(event: number): any {
    console.log('next page ', event);
    this.currentPage = event;
    this.getTableList();
  }

  onLimitChange(event: any): any {
    this.limit = event;
    this.currentPage = 1;
    this.getTableList();
  }

  onExpandChange(index: number, checked: boolean): void {
    if (checked) {
      this.view = index;
      this.getInfo(this.CategoriesList[index]._id);
    } else {
      this.view = -1;
    }
  }

  getInfo(id: any): any {
    this.selectedId = id;
    let url = 'categories/' + id + '/subcategories';
    try {
      this.appService.getMethod(url)
        .subscribe((resp: any) => {
          console.log('admin ', resp);
          this.SubCategories = resp;
        }, (err: any) => {
          console.log('err', err);
          this.nzMessage.error(err.name, err.statusText);
        });
    } catch (e) {
    }
  }

  confirmCategory(data: any, type: number): any {
    this.categoryType = type;
    const url = 'categories/' + data._id + '/delete';
    try {
      this.appService.deleteMethod(url)
        .subscribe((resp: any) => {
          console.log(resp);
          if (resp.status === 200 && this.categoryType === 1) {
            this.nzMessage.success(resp.body.additional_info);
            this.getTableList();
          } else if (resp.status === 200 && this.categoryType === 2) {
            this.nzMessage.success(resp.body.additional_info);
            this.getInfo(this.selectedId);
          }
        },
          (resp: any) => {
            console.log('err', resp);
            this.nzMessage.error(resp.name, resp.statusText);
          });
    } catch (e) {
    }
  }

  cancel(): any { }

  create(type: number, action: string, data?: any): any {
    this.categoryType = type;
    this.actionType = action;
    if (data) {
      this.categoryName = data.name;
      this.updateId = data._id;
      this.isVisibleUpdate = true;
    } else {
      this.isVisible = true;

    }
  }

  updateCategory(): any {
    const url = 'categories/' + this.updateId + '/update';
    try {
      this.appService.patchMethod(url, {name: this.categoryName})
        .subscribe((resp: any) => {
          console.log(resp);
          if (resp.status === 200 && this.categoryType === 1) {
            this.isVisibleUpdate = false;
            this.nzMessage.success(resp.body.additional_info);
            this.getTableList();
          } else if (resp.status === 200 && this.categoryType === 2) {
            this.isVisibleUpdate = false;
            this.nzMessage.success(resp.body.additional_info);
            this.getInfo(this.selectedId);
          }
        },
          (resp: any) => {
            console.log('err', resp);
            this.nzMessage.error(resp.name, resp.statusText);
          });
    } catch (e) {
    }
  }

  handleOk(): void {
    const url = 'categories/new';
    const body = { name: this.categoryName, parent: this.categoryType === 1 ? null : this.selectedId };
    try {
      this.appService.postMethod(url, body)
        .subscribe((resp: any) => {
          console.log(resp);
          this.isVisible = false;
          if (resp.status === 200) {
            this.categoryName = '';
            this.nzMessage.success(resp.body.additional_info);
            if (this.categoryType === 1) {
              this.getTableList();
            } else {
              this.getInfo(this.selectedId);
            }
          }
        },
          (err: any) => {
            console.log('err', err);
            this.nzMessage.error(err.name, err.statusText);
          });
    } catch (e) {
    }
  }

  handleCancel(): void {
    console.log('Button cancel clicked!');
    this.isVisible = false;
  }

  handleCancelUpdate(): void {
    console.log('Button cancel clicked!');
    this.isVisibleUpdate = false;
  }

}
