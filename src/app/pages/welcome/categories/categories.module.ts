import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoriesRoutingModule } from './categories-routing.module';
import { CategoriesComponent } from '../categories/categories.component';
import { AccountSharedModule } from 'src/app/accounts.shared.module';


@NgModule({
  declarations: [CategoriesComponent],
  imports: [
    CommonModule,
    CategoriesRoutingModule,
    AccountSharedModule
  ]
})
export class CategoriesModule { }
