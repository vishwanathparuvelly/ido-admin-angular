import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AppService } from './app.service';

@Injectable()
export class AuthGuardService implements CanActivate {

    constructor(
        private router: Router,
        private appService: AppService
    ) {}

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
        this.appService.checkAuth().subscribe(
            res => {
                console.log('res: ', res);
                if (res.status === 200) {
                    // return true;
                }
            }, err => {
                console.log('err: ', err);
                return false;
            }
        );
    }

}
