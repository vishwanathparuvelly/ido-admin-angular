export const environment = {
  production: true,
  prodUrl: "https://api.belt2door.com/admin",
  uploadUrl: "https://api.belt2door.com/upload",
};
