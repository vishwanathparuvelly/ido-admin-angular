import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SuperAdminComponent } from './super-admin.component';
import { SuperAdminRoutingModule } from './super-admin.routing';
import { basicImportsModule } from 'src/app/basicimports.module';
import { NzLayoutModule } from 'ng-zorro-antd/layout';

@NgModule({
  imports: [
    CommonModule,
    SuperAdminRoutingModule,
    NzLayoutModule,
    basicImportsModule
  ],
  declarations: [SuperAdminComponent]
})
export class SuperAdminModule { }
