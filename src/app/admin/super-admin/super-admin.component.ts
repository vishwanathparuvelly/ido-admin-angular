import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-super-admin',
  templateUrl: './super-admin.component.html',
  styleUrls: ['./super-admin.component.scss']
})
export class SuperAdminComponent implements OnInit {

  selectedTab = 0;
  isTableLoading = false;
  selectedId = '';
  isVisible = false;
  categoryType = 1;
  name = '';
  email = '';
  password = '';
  updateId: any;
  isVisibleUpdate = false;
  tableList = [];
  currentPage = 1;
  limit = this.appService.limit;
  nzPageSizeOptions = this.appService.nzPageSizeOptions;
  count: any = 0;
  accountParamsobj: any = {
    limit: this.limit,
    pageNumber: this.currentPage
  };
  view = -1;
  skip = 0;

  constructor(
    public appService: AppService,
    public nzMessageService: NzMessageService
  ) { }

  ngOnInit() {
    this.getTableList();
  }

  getTableList(): any {
    this.isTableLoading = true;
    let url = 'api/superadmin';
    try {
      this.appService.getMethod(url)
        .subscribe((resp: any) => {
          console.log(resp);
          this.tableList = resp.data.superadmins;
          console.log('this.tableList: ', this.tableList);
          this.count = resp.total;
          this.isTableLoading = false;
        },
          (resp: any) => {
            console.log('err', resp);
            this.isTableLoading = false;
            this.nzMessageService.error(resp.error.message);
          });
    } catch (e) {
    }
  }

  create(): any {
    this.isVisible = true;
  }

  cancel(): any { }

  confirmCategory(data: any, type: number): any {
    // this.categoryType = type;
    // let url = '';
    // if (this.selectedTab === 0) {
    //   url = 'banners/' + data._id + '/delete';
    // } else if (this.selectedTab === 1) {
    //   url = 'wholesale_banners/' + data._id + '/delete';
    // }
    // try {
    //   this.appService.patchMethod(url)
    //     .subscribe((resp: any) => {
    //       console.log(resp);
    //       if (resp.status === 200) {
    //         this.nzMessage.success(resp.body.additional_info);
    //         this.getTableList();
    //       }
    //     },
    //       (resp: any) => {
    //         console.log('err', resp);
    //         this.nzMessage.error(resp.name, resp.statusText);
    //       });
    // } catch (e) {
    // }
  }

  handleOk() {
    let url = 'api/superadmin/admin';
    let body;
    body = { name: this.name, email: this.email, password: this.password };
    try {
      this.appService.postMethod(url, body)
        .subscribe((resp: any) => {
          console.log('resp ', resp);
          this.isVisible = false;
          if (resp.status === 200) {
            this.name = '';
            this.email = '';
            this.password = '';
            this.nzMessageService.success(resp.body.data.message);
            this.getTableList();
          }
        },
          (err: any) => {
            console.log('err', err);
            this.nzMessageService.error(err.error.message);
          });
    } catch (e) {
    }
  }

  handleCancel() {
    this.isVisible = false;
  }

}
