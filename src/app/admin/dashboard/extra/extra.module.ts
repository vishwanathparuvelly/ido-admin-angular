import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExtraRoutingModule } from './extra-routing.module';
import { ExtraComponent } from '../extra/extra.component';
import { basicImportsModule } from 'src/app/basicimports.module';


@NgModule({
  declarations: [ExtraComponent],
  imports: [
    CommonModule,
    basicImportsModule,
    ExtraRoutingModule
  ]
})
export class ExtraModule { }
