import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-extra',
  templateUrl: './extra.component.html',
  styleUrls: ['./extra.component.css']
})
export class ExtraComponent implements OnInit {

  selectedTab = 0;
  isTableLoading = false;
  selectedId = '';
  isVisible = false;
  categoryType = 1;
  name = '';
  sno = '';
  updateId: any;
  isVisibleUpdate = false;
  tableList = [];
  currentPage = 1;
  limit = this.appService.limit;
  nzPageSizeOptions = this.appService.nzPageSizeOptions;
  count: any = 0;
  accountParamsobj: any = {
    limit: this.limit,
    skip: this.currentPage
  };
  view = -1;
  serviceId = new FormControl();
  ServicesList = [];
  isFormVisiable = false;
  isEditing = false;
  myForm: FormGroup;
  isBtnLoading: boolean;
  serviceName = '';
  skip = 0;

  constructor(
    public appService: AppService,
    public nzMessageService: NzMessageService
  ) { }

  ngOnInit() {
    this.myForm = new FormGroup({
      s_no: new FormControl('', Validators.required),
      extra_name: new FormControl('', Validators.required)
    });
    this.getServices();
    this.getTableList();
  }

  getTableList(): any {
    this.isTableLoading = true;
    let url = 'api/admin/services/extra';
    let body = { skip: this.skip, limit: this.limit };
    try {
      this.appService.postMethod(url, body)
        .subscribe((resp: any) => {
          console.log('resp ', resp);
          this.isTableLoading = false;
          this.tableList = resp.body.data.all_extras;
          this.count = resp.body.data.totalCount;
        },
          (resp: any) => {
            console.log('err', resp);
            this.isTableLoading = false;
            this.nzMessageService.error(resp.error.message);
          });
    } catch (e) {
    }
  }

  onNextPage(event: number): any {
    console.log('next page ', event);
    this.view = -1;
    this.currentPage = event;
    this.skip = (this.currentPage - 1) * this.limit;
    this.getTableList();
  }

  onLimitChange(event: any): any {
    this.limit = event;
    this.currentPage = 1;
    this.skip = 0;
    this.getTableList();
  }

  create(): any {
    this.isVisible = true;
  }

  cancel(): any { }

  confirmCategory(data: any) {
    let url = 'api/admin/services/extra/' + data._id;
    try {
      this.appService.deleteMethod(url)
        .subscribe((resp: any) => {
          console.log(resp);
          if (resp.status === 200) {
            this.nzMessageService.success(resp.body.data.message);
            this.currentPage = 1;
            this.skip = 0;
            this.getTableList();
          }
        },
          (resp: any) => {
            console.log('err', resp);
            this.nzMessageService.error(resp.error.message);
          });
    } catch (e) {
    }
  }

  handleOk() {
    let url = 'api/admin/city/create';
    let body;
    body = { city_name: this.name, s_no: this.sno };
    try {
      this.appService.postMethod(url, body)
        .subscribe((resp: any) => {
          console.log('resp ', resp);
          this.isVisible = false;
          if (resp.status === 200) {
            this.name = '';
            this.sno = '';
            this.nzMessageService.success(resp.body.data.message);
            this.getTableList();
          }
        },
          (err: any) => {
            console.log('err', err);
            this.nzMessageService.error(err.error.message);
          });
    } catch (e) {
    }
  }

  handleCancel() {
    this.isVisible = false;
    this.sno = '';
    this.name = '';
  }

  onGetAddForm() {
    this.isFormVisiable = true;
  }

  getServices(): any {
    let url = 'api/admin/services';
    try {
      this.appService.postMethod(url, { skip: 0, limit: 10 })
        .subscribe((resp: any) => {
          console.log('resp ', resp);
          this.ServicesList = resp.body.data.all_mainCategory;
          if (this.ServicesList.length > 0) {
            this.serviceId.setValue(this.ServicesList[0]._id);
            this.serviceName = this.ServicesList[0].name;
          }
        },
          (resp: any) => {
            console.log('err', resp);
            this.isTableLoading = false;
            this.nzMessageService.error(resp.error.message);
          });
    } catch (e) {
    }
  }

  onSubmit() {
    this.isBtnLoading = true;
    let url = 'api/admin/services/extra/create';
    let body = {
      serviceId: this.serviceId.value,
      s_no: this.myForm.controls['s_no'].value,
      extra_name: this.myForm.controls['extra_name'].value
    };
    try {
      this.appService.postMethod(url, body)
        .subscribe(resp => {
          this.isBtnLoading = false;
          this.isFormVisiable = false;
          this.nzMessageService.success(resp.body.data.message);
          this.currentPage = 1;
          this.skip = 0;
          this.getTableList();
        },
          error => {
            this.isBtnLoading = false;
            this.nzMessageService.error(error.error.message);
          })
    } catch (e) {
    }
  }

  onReset() {
    this.isFormVisiable = false;
    this.myForm.reset();
  }

}
