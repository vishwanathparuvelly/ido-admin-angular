import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CityRoutingModule } from './city-routing.module';
import { CityComponent } from '../city/city.component';
import { basicImportsModule } from 'src/app/basicimports.module';


@NgModule({
  declarations: [CityComponent],
  imports: [
    CommonModule,
    basicImportsModule,
    CityRoutingModule
  ]
})
export class CityModule { }
