import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AppService } from 'src/app/app.service';

declare var google: any;
@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.css']
})
export class CityComponent implements OnInit {

  selectedTab = 0;
  isTableLoading = false;
  selectedId = '';
  isVisible = false;
  categoryType = 1;
  name = '';
  sno = '';
  updateId: any;
  isVisibleUpdate = false;
  tableList = [];
  currentPage = 1;
  limit = this.appService.limit;
  nzPageSizeOptions = this.appService.nzPageSizeOptions;
  count: any = 0;
  accountParamsobj: any = {
    limit: this.limit,
    skip: this.currentPage
  };
  view = -1;
  skip = 0;
  Latitude = '17.38';
  Longitude = '78.44';
  address: any;
  isBtnLoading = false;
  isEditing = false;
  cityMap: any;
  editId: any;
  searchCityName = new FormControl(null);
  searchLocalityName = new FormControl(null);

  constructor(
    public appService: AppService,
    public nzMessageService: NzMessageService
  ) { }

  ngOnInit() {
    this.getTableList();
    this.onMapIntilized();
    this.locationOnMap();
  }

  locationOnMap() {
    let center = { lat: parseFloat(this.Latitude), lng: parseFloat(this.Longitude) }
    this.cityMap = new google.maps.Map(document.getElementById('map'), {
      center: center,
      zoom: 10,
      fullscreenControl: true
    });
  }

  getTableList(): any {
    this.isTableLoading = true;
    let url = 'api/admin/city';
    let body = { skip: this.skip, limit: this.limit,
      search_input: this.searchCityName.value !== null ? this.searchCityName.value : '',
       localities_input: this.searchLocalityName.value !== null ? this.searchLocalityName.value : '' };
    try {
      this.appService.postMethod(url, body)
        .subscribe((resp: any) => {
          console.log('resp ', resp);
          this.isTableLoading = false;
          this.tableList = resp.body.data.all_city;
          this.count = resp.body.data.totalCount;
        },
          (resp: any) => {
            console.log('err', resp);
            this.isTableLoading = false;
            this.nzMessageService.error(resp.error.message);
          });
    } catch (e) {
    }
  }

  onNextPage(event: number): any {
    console.log('next page ', event);
    this.view = -1;
    this.currentPage = event;
    this.skip = (this.currentPage - 1) * this.limit;
    this.getTableList();
  }

  onLimitChange(event: any): any {
    this.limit = event;
    this.currentPage = 1;
    this.skip = 0;
    this.getTableList();
  }

  create(type, data?): any {
    console.log('data: ', data);
    if (type === 1) {
      this.isVisible = true;
      this.isEditing = false;
    } else if (type === 2) {
      this.isVisible = true;
      this.isEditing = true;
      this.sno = data.s_no;
      this.name = data.city_name;
      this.Latitude = data.lat;
      this.Longitude = data.lng;
      this.editId = data._id;
    }
    setTimeout(() => {
      this.locationOnMap();
    }, 500);
  }

  cancel(): any { }

  confirmCategory(data: any): any {
    let url = 'api/admin/city/' + data._id;
    try {
      this.appService.deleteMethod(url)
        .subscribe((resp: any) => {
          console.log(resp);
          if (resp.status === 200) {
            this.nzMessageService.success(resp.body.data.message);
            this.getTableList();
          }
        },
          (resp: any) => {
            console.log('err', resp);
            this.nzMessageService.error(resp.error.message);
          });
    } catch (e) {
    }
  }

  handleOk() {
    let url = this.isEditing ? 'api/admin/city' : 'api/admin/city/create';
    let body;
    this.isBtnLoading = true;
    body = { city_name: this.name, s_no: this.sno, lat: this.Latitude, lng: this.Longitude };
    if (this.isEditing) {
      body.cityId = this.editId;
    }
    if (this.Latitude && this.Longitude) {
      try {
        if (this.isEditing) {
          this.appService.putMethod(url, body)
          .subscribe((resp: any) => {
            console.log('resp ', resp);
            this.isBtnLoading = false;
            this.isVisible = false;
            if (resp.status === 200) {
              this.name = '';
              this.sno = '';
              this.nzMessageService.success(resp.body.data.message);
              this.getTableList();
            }
          },
            (err: any) => {
              this.isBtnLoading = false;
              this.nzMessageService.error(err.error.message);
            });
        }
        if (!this.isEditing) {
          this.appService.postMethod(url, body)
          .subscribe((resp: any) => {
            console.log('resp ', resp);
            this.isBtnLoading = false;
            this.isVisible = false;
            if (resp.status === 200) {
              this.name = '';
              this.sno = '';
              this.nzMessageService.success(resp.body.data.message);
              this.getTableList();
            }
          },
            (err: any) => {
              this.isBtnLoading = false;
              this.nzMessageService.error(err.error.message);
            });
        }
      } catch (e) {
      }
    } else {
      this.isBtnLoading = false;
      this.nzMessageService.error('Please enter address');
    }
    
  }

  handleCancel() {
    this.isVisible = false;
    this.isBtnLoading = false;
    this.sno = '';
    this.name = '';
    this.address = '';
  }

  onMapIntilized() {
    console.log('address: ', this.address);
    const input: any = document.getElementById('pac-input');
    const autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.setFields(
      ['address_components', 'geometry', 'icon', 'name']);
    autocomplete.addListener('place_changed', () => {
      const place = autocomplete.getPlace();
      if (!place.geometry) {
        window.alert('No details available for input: \'' + place.name + '\'');
        return;
      } else {
        this.Latitude = place.geometry.location.lat();
        this.Longitude = place.geometry.location.lng();
        this.locationOnMap();
      }
    });
  }

  searchCity(event) {
    console.log('event: ', event);
    setTimeout(() => {
      this.skip = 0;
      this.currentPage = 1;
      if (event.length > 2) {
        this.getTableList();
      } else if (event.length === 0) {
        this.getTableList();
      }
    }, 500);
  }

}
