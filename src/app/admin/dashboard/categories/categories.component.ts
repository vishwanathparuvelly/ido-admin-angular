import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AppService } from 'src/app/app.service';
// import { UploadFile } from 'ng-zorro-antd/upload';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  selectedTab = 1;
  isTableLoading = false;
  selectedId = '';
  isVisible = false;
  categoryType = 1;
  name = '';
  updateId: any;
  isVisibleUpdate = false;
  tableList = [];
  currentPage = 1;
  limit = this.appService.limit;
  nzPageSizeOptions = this.appService.nzPageSizeOptions;
  count: any = 0;
  accountParamsobj: any = {
    limit: this.limit,
    skip: this.currentPage
  };
  view = -1;
  fileList!: File;
  previewImage: string | undefined = '';
  previewVisible = false;
  files: FileList;
  description: '';
  s_no: '';
  fileInput = null;
  fileToUpload: File;
  categoryList = [];
  mainCategory = '';
  skip = 0;
  isBtnLoading = false;
  isEditing = false;
  ImageLink: any;
  ImageEditing = false;
  editId: string | Blob;

  // handlePreview = async (file: NzUploadFile) => {
  //   console.log('file: ', file);
  //   if (!file.url && !file.preview) {
  //     file.preview = await getBase64(file.originFileObj!);
  //   }
  //   this.files = file;
  //   console.log('this.files: ', this.files);
  //   this.previewImage = file.url || file.preview;
  //   this.previewVisible = true;
  // }

  constructor(
    public appService: AppService,
    private nzMessage: NzMessageService
  ) { }

  ngOnInit(): void {
    this.getTableList();
  }

  getTableList(): any {
    this.isTableLoading = true;
    let url = '';
    if (this.selectedTab === 1) {
      url = 'api/admin/mainCategory';
    } else if (this.selectedTab === 2) {
      url = 'api/admin/subCategory';
    }
    let body = {
      skip: this.skip,
      limit: this.limit
    }
    console.log('body: ', body);
    try {
      this.appService.postMethod(url, body)
        .subscribe((resp: any) => {
          console.log('resp.body ', resp.body);
          this.isTableLoading = false;
          if (this.selectedTab === 1) {
            this.tableList = resp.body.data.all_mainCategory;
            this.categoryList = resp.body.data.all_mainCategory;
          } else if (this.selectedTab === 2) {
            this.tableList = resp.body.data.all_subCategory;
          }
          this.count = resp.body.data.totalCount;
        },
          (resp: any) => {
            console.log('err', resp);
            this.isTableLoading = false;
            this.nzMessage.error(resp.error.message);
          });
    } catch (e) {
    }
  }

  onNextPage(event: number): any {
    console.log('next page ', event);
    this.view = -1;
    this.currentPage = event;
    this.skip = (this.currentPage - 1) * this.limit;
    this.getTableList();
  }

  onLimitChange(event: any): any {
    console.log('event limit: ', event);
    this.limit = event;
    this.currentPage = 1;
    this.skip = 0;
    this.getTableList();
  }

  onExpandChange(index: number, checked: boolean): void {
    if (checked) {
      this.view = index;
      // this.getInfo(this.tableList[index]._id);
    } else {
      this.view = -1;
    }
  }

  cancel(): any { }

  confirmCategory(data: any, type: number): any {
    this.categoryType = type;
    let url = '';
    if (this.selectedTab === 1) {
      url = 'api/admin/mainCategory/' + data._id;
    } else if (this.selectedTab === 2) {
      url = 'api/admin/subCategory/' + data._id;
    }
    try {
      this.appService.deleteMethod(url)
        .subscribe((resp: any) => {
          console.log(resp);
          if (resp.status === 200) {
            this.nzMessage.success(resp.body.data.message);
            this.skip = 0;
            this.limit = 10;
            this.currentPage = 1;
            this.getTableList();
          }
        },
          (resp: any) => {
            console.log('err', resp);
            this.nzMessage.error(resp.error.message);
          });
    } catch (e) {
    }
  }

  tabChange(event: number): any {
    this.selectedTab = event;
    this.selectedId = '';
    this.skip = 0;
    this.limit = 10;
    this.currentPage = 1;
    this.getTableList();
  }

  create(type: number, data?, catType?) {
    console.log('data: ', data);
    if (type === 1) {
      this.isVisible = true;
      this.isEditing = false;
    } else if (type === 2) {
      this.isVisible = true;
      this.isEditing = true;
      this.description = data.description;
      this.name = data.name;
      this.s_no = data.s_no;
      this.ImageLink = data.image;
      if (catType) {
        this.mainCategory = data.category_id;
      }
      this.editId = data._id;
    }
  }

  onSubmit(type: number): void {
    let url = '';
    let body;
    if (type === 1) {
      url = this.isEditing ? 'api/admin/mainCategory' : 'api/admin/mainCategory/create';
    } else if (type === 2) {
      url = this.isEditing ? 'api/admin/subCategory' : 'api/admin/subCategory/create';
    }
    body = { 
      name: this.name,
      s_no: this.s_no,
      description: this.description
     };
     const formData: FormData = new FormData();
    formData.append('s_no', this.s_no);
    formData.append('name', this.name);
    formData.append('description', this.description);
    if (this.selectedTab === 2) {
      formData.append('category_id', this.mainCategory);
    }
    if (this.fileToUpload) {
      formData.append('image', this.fileToUpload);
    }
    if (this.isEditing && type === 1) {
      formData.append('mainCategoryId', this.editId);
    }
    if (this.isEditing && type === 2) {
      formData.append('subCategoryId', this.editId);
    }
    
    console.log('this.fileList: ', this.fileList);
    try {
      if (this.isEditing) {
        this.appService.filePutMethod(url, formData) 
        .subscribe((resp: any) => {
          console.log(resp);
          this.isVisible = false;
          if (resp.status === 200) {
            this.onReset();
            this.nzMessage.success(resp.body.data.message);
            this.skip = 0;
            this.currentPage = 1;
            this.limit = 10;
            this.getTableList();
          }
        },
          (err: any) => {
            console.log('err', err);
            this.nzMessage.error(err.error.message);
          });
      } else {
        this.appService.fileMethod(url, formData) 
        .subscribe((resp: any) => {
          console.log(resp);
          this.isVisible = false;
          if (resp.status === 200) {
            this.onReset();
            this.nzMessage.success(resp.body.data.message);
            this.skip = 0;
            this.currentPage = 1;
            this.limit = 10;
            this.getTableList();
          }
        },
          (err: any) => {
            console.log('err', err);
            this.nzMessage.error(err.error.message);
          });
      }
    } catch (e) {
    }
  }

  handleCancel(): void {
    console.log('Button cancel clicked!');
    this.isVisible = false;
    this.s_no = '';
    this.name = '';
    this.description = '';
    this.fileInput = null;
    this.mainCategory = '';
    let ff: File;
    this.fileToUpload = ff;
  }

  onChangeStatus(data: any): any {
    console.log('data', data);
    let url = '';
    if (this.selectedTab === 1) {
      url = 'banners/' + data._id + '/update_status';
    } else if (this.selectedTab === 2) {
      url = 'wholesale_banners/' + data._id + '/update_status';
    }
    try {
      this.appService.postMethod(url, { status: data.status === 'INACTIVE' ? 'ACTIVE' : 'INACTIVE' })
        .subscribe(resp => {
          if (resp.success) {
            this.nzMessage.success(resp.body.data.message);
            this.getTableList();
          } else {
          }
        },
          (err: any) => {
            this.getTableList();
            this.nzMessage.error(err.error.message);
          });
    } catch (e) { }
  }

  handleChange({ file, fileList }: any): void {
    console.log('file: ', file);
    const status = file.status;
    console.log('status: ', status);
    if (status !== 'uploading') {
      console.log(file, fileList);
    }
    this.fileList = file;
    console.log('this.fileList: ', this.fileList);
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    var reader = new FileReader();
    // this.imagePath = file;
    reader.readAsDataURL(this.fileToUpload);
    reader.onload = (_event) => {
      this.ImageLink = reader.result;
    }
    if (this.isEditing) {
      this.ImageEditing = true;
    }
    console.log('fileToUpload ', this.fileToUpload);
  }

  onReset() {
    this.isVisible = false;
    this.isEditing = false;
    this.s_no = '';
    this.name = '';
    this.description = '';
    this.fileInput = null;
    this.mainCategory = '';
    let ff: File;
    this.fileToUpload = ff;
  }
}
