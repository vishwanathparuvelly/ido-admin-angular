import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoriesRoutingModule } from './categories-routing.module';
import { CategoriesComponent } from '../categories/categories.component';
import { basicImportsModule } from 'src/app/basicimports.module';


@NgModule({
  declarations: [CategoriesComponent],
  imports: [
    CommonModule,
    basicImportsModule,
    CategoriesRoutingModule
  ]
})
export class CategoriesModule { }
