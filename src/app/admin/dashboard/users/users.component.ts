import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  searchName = new FormControl('');
  searchPhoneNumber = new FormControl('');
  userBlocked = new FormControl(null);
  currentPage = 1;
  limit = this._appService.limit;
  nzPageSizeOptions = this._appService.nzPageSizeOptions;
  count: any = 0;
  accountParamsobj: any = {
    limit: this.limit,
    skip: this.currentPage
  };
  isTableLoading: boolean;
  tableList = [];
  skip = 0;
  view = -1;
  UserData: any;
  isSelected = true;
  selectedTab = 1;
  bookingList = [];
  bookingSkip = 0;
  bookingCurrentPage = 1;
  bookingCount = 0;

  constructor(
    private nzMessageService: NzMessageService,
    private _appService: AppService,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.getTableList();
  }

  searchPhone(event) {
    console.log('event: ', event);
    setTimeout(() => {
      this.skip = 0;
      this.currentPage = 1;
      if (event.length > 2) {
        this.getTableList();
      } else if (event.length === 0) {
        this.getTableList();
      }
    }, 500);
  }

  searchByName(event) {
    console.log('event: ', event);
    setTimeout(() => {
      this.skip = 0;
      this.currentPage = 1;
      if (event.length > 2) {
        this.getTableList();
      } else if (event.length === 0) {
        this.getTableList();
      }
    }, 500);
  }

  userStatus(event) {
    console.log('event: ', event);
    this.skip = 0;
    this.currentPage = 1;
    this.getTableList();
  }

  getTableList(): any {
    this.isTableLoading = true;
    let body = {
      "search_input": this.searchName.value,
      "phoneNumber": this.searchPhoneNumber.value,
      "userBlocked": this.userBlocked.value !== null ? new Boolean(this.userBlocked.value) : '',
      "skip": this.skip,
      "limit": this.limit
    };
    let url = 'api/admin/users';
    try {
      this._appService.postMethod(url, body)
        .subscribe((resp: any) => {
          console.log('resp ', resp.body);
          this.isTableLoading = false;
          this.tableList = resp.body.data.users;
          this.count = resp.body.data.totalCount;
        },
          (resp: any) => {
            console.log('err', resp);
            this.isTableLoading = false;
            this.nzMessageService.error(resp.error.message);
          });
    } catch (e) {
    }
  }

  onNextPage(event: number): any {
    console.log('next page ', event);
    this.view = -1;
    this.currentPage = event;
    this.skip = (this.currentPage - 1) * this.limit;
    this.getTableList();
  }

  onLimitChange(event: any): any {
    this.limit = event;
    this.currentPage = 1;
    this.skip = 0;
    this.getTableList();
  }

  detailView(data, AI) {
    if (this.view !== AI) {
      this.view = AI;
      let url = 'api/admin/users/' + data._id;
      try {
        this._appService.getMethod(url)
          .subscribe(resp => {
            console.log('resp: ', resp);
            this.UserData = resp.data.user;
            this.isSelected = true;
            this.getBookingList();
          },
            (err: any) => {
              console.log('err: ', err);
              this.nzMessageService.error(err.error.message);
            });
      } catch (e) { }
    } else {
      this.view = -1;
    }
  }

  onTab(event) {
    this.isSelected = event;
    this.getBookingList();
  }

  getBookingList(): any {
    // this.isTableLoading = true;
    let url;
    if (this.isSelected) {
      url = 'api/admin/users/onGoingBooking/' + this.UserData._id;
    } else if (!this.isSelected) {
      url = 'api/admin/users/pastBooking/' + this.UserData._id;
    }
    let body = {
      skip: this.bookingSkip, limit: this.limit
    };
    try {
      this._appService.postMethod(url, body)
        .subscribe((resp: any) => {
          console.log('resp ', resp);
          // this.isTableLoading = false;
          this.bookingList = resp.body.data.booking;
          this.bookingCount = resp.body.data.totalCount;
        },
          (resp: any) => {
            console.log('err', resp);
            // this.isTableLoading = false;
            this.nzMessageService.error(resp.error.message);
          });
    } catch (e) {
    }
  }

  onBookingNextPage(event: number): any {
    console.log('next page ', event);
    this.bookingCurrentPage = event;
    this.bookingSkip = (this.bookingCurrentPage - 1) * this.limit;
    this.getBookingList();
  }

  onBookingLimitChange(event: any): any {
    this.limit = event;
    this.bookingCurrentPage = 1;
    this.bookingSkip = 0;
    this.getBookingList();
  }

}
