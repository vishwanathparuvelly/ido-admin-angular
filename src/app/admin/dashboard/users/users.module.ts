import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from '../users/users.component';
import { basicImportsModule } from 'src/app/basicimports.module';


@NgModule({
  declarations: [UsersComponent],
  imports: [
    CommonModule,
    basicImportsModule,
    UsersRoutingModule
  ]
})
export class UsersModule { }
