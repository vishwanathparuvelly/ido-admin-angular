import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AppService } from 'src/app/app.service';
import * as moment from 'moment';

@Component({
  selector: 'app-partner',
  templateUrl: './partner.component.html',
  styleUrls: ['./partner.component.css']
})
export class PartnerComponent implements OnInit {

  isFormVisiable = false;
  citiesList = [];
  cityId = new FormControl('');
  search_input = new FormControl('');
  localities = new FormControl('');
  subCategoryId = new FormControl('');
  mainCategoryId = new FormControl('');
  currentPage = 1;
  limit = this._appService.limit;
  nzPageSizeOptions = this._appService.nzPageSizeOptions;
  count: any = 0;
  accountParamsobj: any = {
    limit: this.limit,
    skip: this.currentPage
  };
  isTableLoading: boolean;
  tableList = [];
  view = -1;
  categoryList = [];
  subCategoryList = [];
  LocalitiesList = [];
  isEditing = false;
  myForm: FormGroup;
  date = null;
  skip = 0;
  bookingData: any;
  fileInput = null;
  fileToUpload: File;
  ImageLink: any;
  tabSelection = 0;
  KYCData: any;
  isEditAddress = false;
  isEditIdentity = false;
  addressForm: FormGroup;
  documentForm: FormGroup;
  IdentityType = [
    {
      name: 'Aadhaar', value: '1'
    },
    {
      name: 'PAN Card', value: '2'
    },
    {
      name: 'Driving License', value: '3'
    }
  ];
  partnerData: any;
  subCategory = new FormControl([]);
  isVisible = false;
  finalSubCategoryNames: any[];
  selectedAuthControlsData: any;
  selectedauth: FormGroup;

  constructor(
    private nzMessageService: NzMessageService,
    public _appService: AppService,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.myForm = new FormGroup({
      name: new FormControl('', Validators.required),
      phoneNumber: new FormControl('', [Validators.required, Validators.maxLength(10)]),
      email: new FormControl('', [Validators.required, Validators.email]),
      date: new FormControl(null, Validators.required),
      cityId: new FormControl(null, Validators.required),
      localities: new FormControl([], Validators.required),
      skills: this.fb.array([]),
      fileInput: new FormControl(null)
    });
    this.addressForm = new FormGroup({
      flatNo: new FormControl(null),
      localityName: new FormControl(null),
      pinCode: new FormControl(null),
      city: new FormControl(null),
      state: new FormControl(null)
    });
    this.documentForm = new FormGroup({
      id: new FormControl(null),
      type: new FormControl(null),
      name: new FormControl(null),
      fileInput: new FormControl('')
    });
    this.addSkills();
    this.getTableList();
    this.getCities();
    this.getCategories();
    this.mainCategoryId.valueChanges.subscribe(mc => {
      const mcv = mc;
      console.log('mcv: ', mcv);
      if (mcv !== null) {
        const ind = this.categoryList.findIndex(c => c._id === mcv);
        this.subCategoryList = this.categoryList[ind].subCategory;
        if (this.subCategoryList.length > 0) {
          this.subCategoryId.setValue(this.subCategoryList[0]._id);
        }
      }
      this.limit = 10;
      this.currentPage = 1;
      this.skip = 0;
      this.getTableList();
    });
    this.subCategoryId.valueChanges.subscribe(sc => {
      const scv = sc;
      this.limit = 10;
      this.currentPage = 1;
      this.skip = 0;
      this.getTableList();
    });
    this.localities.valueChanges.subscribe(lo => {
      const lov = lo;
      this.limit = 10;
      this.currentPage = 1;
      this.skip = 0;
      this.getTableList();
    });
    this.cityId.valueChanges.subscribe(ci => {
      const civ = ci;
      console.log('civ: ', civ);
      if (civ !== null) {
        const ind = this.citiesList.findIndex(c => c._id === civ);
        this.LocalitiesList = this.citiesList[ind].localities;
        if (this.LocalitiesList.length > 0) {
          this.localities.setValue(this.LocalitiesList[0]._id);
        }
      }
      this.limit = 10;
      this.currentPage = 1;
      this.skip = 0;
      this.getTableList();
    });
    this.myForm.controls['cityId'].valueChanges.subscribe(ci => {
      const civ = ci;
      console.log('civ: ', civ);
      if (civ !== null) {
        this.myForm.controls['localities'].setValue([]);
        const ind = this.citiesList.findIndex(c => c._id === civ);
        this.LocalitiesList = this.citiesList[ind].localities;
      }
    });
    // this.myForm.controls['mainCategoryId'].valueChanges.subscribe(mc => {
    //   const mcv = mc;
    //   console.log('mcv: ', mcv);
    //   if (mcv !== null) {
    //     this.myForm.controls['subCategory'].setValue([]);
    //     const ind = this.categoryList.findIndex(c => c._id === mcv);
    //     this.subCategoryList = this.categoryList[ind].subCategory;
    //   }
    // });
    this.myForm.controls['date'].valueChanges.subscribe(mc => {
      const mcv = mc;
      const dat = new Date(mcv);
      console.log('dat: ', moment(mcv).format('l'));
    });
    this.search_input.valueChanges.subscribe(sc => {
      const scv = sc;
      setTimeout(() => {
        this.limit = 10;
        this.currentPage = 1;
        this.skip = 0;
        this.getTableList();
      }, 500);
    });
  }

  get skills(): FormArray {
    return this.myForm.get("skills") as FormArray
  }

  newSkill(): FormGroup {
    return this.fb.group({
      mainCategoryId: new FormControl(null, Validators.required),
      Sub_Category_ID_Array: [[]],
      Sub_Category_Names: [[]]
    })
  }

  addSkills() {
    this.skills.push(this.newSkill());
  }

  removeSkill(i: number) {
    this.skills.removeAt(i);
  }

  changeCategory(skill: FormGroup, index) {
    console.log('skill: ', skill);
    const f = <FormArray>this.myForm.controls['skills'].value;
    console.log('f[index]: ', f[index]);
    const ind = this.categoryList.findIndex(c => c._id === f[index].mainCategoryId);
    this.subCategoryList = this.categoryList[ind].subCategory;
    console.log('this.subCategoryList: ', this.subCategoryList);
    this.selectedauth = skill;
    console.log('f.at(index): ', f[index].subCategory);
  }

  getCities(): any {
    let url = 'api/admin/city';
    try {
      this._appService.postMethod(url, { skip: 0, limit: 10 })
        .subscribe((resp: any) => {
          console.log('resp ', resp);
          this.citiesList = resp.body.data.all_city;
          if (this.citiesList.length > 0) {
            // this.cityId.setValue(this.citiesList[0]._id);
            this.LocalitiesList = this.citiesList[0].localities;
            // if (this.LocalitiesList.length > 0) {
            //   this.localities.setValue(this.LocalitiesList[0]._id);
            // }
          }
        },
          (resp: any) => {
            console.log('err', resp);
            this.nzMessageService.error(resp.error.message);
          });
    } catch (e) {
    }
  }

  getCategories(): any {
    let url = 'api/admin/mainCategory';
    try {
      this._appService.postMethod(url, { skip: 0, limit: 10 })
        .subscribe((resp: any) => {
          console.log('resp.body ', resp.body);
          this.categoryList = resp.body.data.all_mainCategory;
          if (this.categoryList.length > 0) {
            // this.mainCategoryId.setValue(this.categoryList[0]._id);
            this.subCategoryList = this.categoryList[0].subCategory;
          }
        },
          (resp: any) => {
            console.log('err', resp);
            this.nzMessageService.error(resp.error.message);
          });
    } catch (e) {
    }
  }

  getTableList(): any {
    this.isTableLoading = true;
    let body = {
      "search_input": this.search_input.value,
      "cityId": this.cityId.value,
      "localities": this.localities.value,
      "subCategoryId": this.subCategoryId.value,
      "mainCategoryId": this.mainCategoryId.value,
      "skip": this.skip,
      "limit": this.limit
    };
    let url = 'api/admin/partner';
    try {
      this._appService.postMethod(url, body)
        .subscribe((resp: any) => {
          console.log('resp ', resp.body);
          this.isTableLoading = false;
          this.tableList = resp.body.data.partner;
          this.count = resp.body.data.totalDocuments;
        },
          (resp: any) => {
            console.log('err', resp);
            this.isTableLoading = false;
            this.nzMessageService.error(resp.error.message);
          });
    } catch (e) {
    }
  }

  onNextPage(event: number): any {
    console.log('next page ', event);
    this.view = -1;
    this.currentPage = event;
    this.skip = (this.currentPage - 1) * this.limit;
    this.getTableList();
  }

  onLimitChange(event: any): any {
    this.limit = event;
    this.currentPage = 1;
    this.skip = 0;
    this.getTableList();
  }

  cancel(): any { }

  confirmCategory(data: any): any {
    let url = 'api/admin/partner/' + data._id;
    try {
      this._appService.deleteMethod(url)
        .subscribe((resp: any) => {
          console.log(resp);
          if (resp.status === 200) {
            this.nzMessageService.success(resp.body.data.message);
            this.currentPage = 1;
            this.skip = 0;
            this.getTableList();
          }
        },
          (resp: any) => {
            console.log('err', resp);
            this.nzMessageService.error(resp.error.message);
          });
    } catch (e) {
    }
  }

  onChangeStatus(data: any): any {
    console.log('data', data);
    let url = '';
    url = 'api/admin/partner/change_status';
    let body = { approved_status: !data.approved_status, partnerId: data._id };
    console.log('body: ', body);
    try {
      this._appService.putMethod(url, body)
        .subscribe(resp => {
          this.nzMessageService.success('Status updated successfully');
          this.getTableList();
        },
          (err: any) => {
            this.getTableList();
            this.nzMessageService.error(err.error.message);
          });
    } catch (e) { }
  }

  cancelStatus() { }

  onReset() {
    this.myForm.reset();
    this.isFormVisiable = false;
  }

  onSubmit() {
    console.log('form value ', this.myForm.value);
    let url = '';
    let body;
    if (!this.isEditing) {
      url = 'api/admin/partner/create';
      body = {
        name: this.myForm.controls['name'].value,
        phoneNumber: this.myForm.controls['phoneNumber'].value,
        city_id: this.myForm.controls['cityId'].value,
        dob: moment(this.myForm.controls['date'].value).format('DD/MM/YYYY'),
        email: this.myForm.controls['email'].value,
        localities: this.myForm.controls['localities'].value,
        skills: this.myForm.controls['skills'].value
      };
    } else if (this.isEditing) {
      url = 'api/admin/partner';
      body = {
        name: this.myForm.controls['name'].value,
        phoneNumber: this.myForm.controls['phoneNumber'].value,
        city_id: this.myForm.controls['cityId'].value,
        dob: moment(this.myForm.controls['date'].value).format('DD/MM/YYYY'),
        email: this.myForm.controls['email'].value,
        localities: this.myForm.controls['localities'].value,
        skills: this.myForm.controls['skills'].value,
        partnerId: this.partnerData._id,
        profilePic: this.ImageLink
      };
    }
    console.log('body: ', body);
    try {
      this._appService.postMethod(url, body)
        .subscribe(resp => {
          console.log('resp: ', resp);
          if (!this.isEditing) {
            this.nzMessageService.success(resp.body.data.messagge);
          } else if (this.isEditing) {
            this.nzMessageService.success('Partner updated');
          }
          this.ImageLink = '';
          this.onReset();
          this.skip = 0;
          this.getTableList();
        },
          (err: any) => {
            console.log('err: ', err);
            this.nzMessageService.error(err.error.message);
          });
    } catch (e) { }
  }

  onGetAddForm() {
    this.isEditing = false;
    this.isFormVisiable = true;
  }

  detailView(data, AI) {
    this.tabSelection = 0;
    if (this.view !== AI) {
      this.view = AI;
      let url = 'api/admin/partner/' + data._id;
      try {
        this._appService.getMethod(url)
          .subscribe(resp => {
            this.bookingData = resp.data.partner;
            console.log('this.bookingData: ', this.bookingData);
            this.ImageLink = this.bookingData.profilePic;
          },
            (err: any) => {
              console.log('err: ', err);
              this.nzMessageService.error(err.error.message);
            });
      } catch (e) { }
    } else {
      this.view = -1;
    }
  }

  handleFileInput(files: FileList, type) {
    this.fileToUpload = files.item(0);
    console.log('fileToUpload ', this.fileToUpload); var reader = new FileReader();
    // this.imagePath = file;
    reader.readAsDataURL(this.fileToUpload);
    // reader.onload = (_event) => {
    //   this.ImageLink = reader.result;
    // }
    // if (this.isEditing) {
    //   this.ImageEditing = true;
    // }
    this.uploadAPI(type);
  }

  uploadAPI(type) {
    let url = 'api/upload';
    const formData: FormData = new FormData();
    formData.append('file', this.fileToUpload);
    try {
      this._appService.fileMethod(url, formData)
        .subscribe(resp => {
          console.log('resp: ', resp);
          this.ImageLink = resp.body.data.image;
          if (type === 'PIC') {
            this.bookingData.kyc.passportPic = resp.body.data.image;
          }
          console.log('this.ImageLink: ', this.ImageLink);
        },
          (err: any) => {
            console.log('err: ', err);
            this.nzMessageService.error(err.error.message);
          });
    } catch (e) { }
  }

  update(type, data) {
    let url = '';
    let body;
    if (type === 'PICTURE') {
      url = 'api/admin/partner/kyc/kyc-passport';
      body = { partnerId: data._id, passportImage_url: this.ImageLink }
    }
    try {
      this._appService.putMethod(url, body)
        .subscribe(resp => {
          console.log('resp: ', resp);
          this.nzMessageService.success(resp.body.data.message);
          this.view = -1;
          this.ImageLink = '';
          this.getTableList();
        },
          (err: any) => {
            console.log('err: ', err);
            this.nzMessageService.error(err.error.message);
          });
    } catch (e) { }
  }

  tabChange(data) {
    console.log('data: ', data);
    this.isEditAddress = false;
    this.isEditIdentity = false;
    if (this.tabSelection === 1) {
      let url = 'api/admin/partner/kyc/' + data._id;
      try {
        this._appService.getMethod(url)
          .subscribe(resp => {
            console.log('resp: ', resp);
            this.KYCData = resp.data.kyc;
            console.log('this.KYCData: ', this.KYCData);
          },
            (err: any) => {
              console.log('err: ', err);
              this.nzMessageService.error(err.error.message);
            });
      } catch (e) { }
    }
  }

  editKYC(type: string) {
    if (type === 'ADDRESS') {
      this.addressForm.get('flatNo').patchValue(this.KYCData.address.flatNo);
      this.addressForm.get('localityName').patchValue(this.KYCData.address.localityName);
      this.addressForm.get('city').patchValue(this.KYCData.address.city);
      this.addressForm.get('pinCode').patchValue(this.KYCData.address.pinCode);
      this.addressForm.get('state').patchValue(this.KYCData.address.state);
      // this.ImageLink = this.KYCData.address.attachment;
      this.isEditAddress = true;
    } else if (type === 'IDENTITY') {
      console.log('this.KYCData.document: ', this.KYCData.document);
      this.documentForm.get('type').patchValue(this.KYCData.document.type.toString());
      this.documentForm.get('name').patchValue(this.KYCData.document.name);
      this.documentForm.get('id').patchValue(this.KYCData.document.id);
      this.ImageLink = this.KYCData.document.attachment;
      this.isEditIdentity = true;
    }
  }

  onUpdate(type) {
    let url = '';
    let body;
    if (type === 'ADDRESS') {
      url = 'api/admin/partner/kyc-addressproof';
      body = {
        flatNo: this.addressForm.get('flatNo').value,
        localityName: this.addressForm.get('localityName').value,
        city: this.addressForm.get('city').value,
        pinCode: this.addressForm.get('pinCode').value,
        state: this.addressForm.get('state').value,
        partnerId: this.KYCData.partnerId
      }
    } else if (type === 'IDENTITY') {
      url = 'api/admin/partner/kyc-identity';
      body = {
        type: this.documentForm.get('type').value,
        name: this.documentForm.get('name').value,
        id: this.documentForm.get('id').value,
        attachment: this.ImageLink,
        partnerId: this.KYCData.partnerId
      }
    }
    console.log('body: ', body);
    try {
      this._appService.putMethod(url, body)
        .subscribe(resp => {
          this.isEditAddress = false;
          this.isEditIdentity = false;
          if (type === 'ADDRESS') {
            this.nzMessageService.success('Partner Address Proof updated successfully');
          } else if (type === 'IDENTITY') {
            this.nzMessageService.success('Partner Identity updated successfully');
          }
          this.view = -1;
          this.ImageLink = '';
          this.getTableList();
        },
          (err: any) => {
            this.getTableList();
            this.nzMessageService.error(err.error.message);
          });
    } catch (e) { }
  }

  editPartner(data) {
    console.log('data: ', data);
    this.isFormVisiable = true;
    this.isEditing = true;
    this.partnerData = data;
    // this.myForm = new FormGroup({
    //   name: new FormControl('', Validators.required),
    //   phoneNumber: new FormControl('', [Validators.required, Validators.maxLength(10)]),
    //   email: new FormControl('', [Validators.required, Validators.email]),
    //   date: new FormControl(null, Validators.required),
    //   cityId: new FormControl(null, Validators.required),
    //   localities: new FormControl([], Validators.required),
    //   skills: this.fb.array([])
    // });
    this.myForm.get('name').patchValue(data.name, { emitEvent: false });
    this.myForm.get('phoneNumber').patchValue(data.phoneNumber, { emitEvent: false });
    this.myForm.get('email').patchValue(data.email, { emitEvent: false });
    if (data.dob && data.dob !== '') {
      const d = data.dob.split('/');
      const nd = d[2] + '-' + d[1] + '-' + d[0];
      console.log('nd: ', nd);
      this.myForm.get('date').patchValue(nd, { emitEvent: false });
    }
    this.myForm.get('cityId').patchValue(data.city_id, { emitEvent: false });
    this.myForm.get('localities').patchValue(data.localities, { emitEvent: false });
    console.log('data.skills.length: ', data.skills.length);
    if (data.skills.length > 0) {
      for (let i = 0; i < data.skills.length - 1; i++) {
        this.addSkills();
      }
    }
    this.ImageLink = data.profilePic;
    this.myForm.get('skills').patchValue(data.skills);
    // setTimeout(() => {
    //   const ass = this.myForm.get('skills') as FormArray;
    //   console.log('ass: ', ass);
    //   data.skills.forEach((sk, i) => {
    //     console.log('sk: ', sk);
    //     console.log('i: ', i);
    //     ass.at(i).get('mainCategoryId').patchValue(sk.mainCategoryId, { emitEvent: false });
    //     ass.at(i).get('subCategory').patchValue(sk.subCategory, { emitEvent: false });
    //   });
    //   const ass1 = this.myForm.get('skills') as FormArray;
    //   console.log('ass1: ', ass1);
    // }, 1000);
    // this.myForm.get('localities').patchValue();
  }

  handleCancel() {
    this.isVisible = false;
    this.subCategory.reset();
  }

  onSendData() {
    if (this.selectedauth == this.selectedAuthControlsData) {
      this.selectedAuthControlsData.patchValue({
        Sub_Category_ID_Array: this.subCategory.value,
      })
      this.getCookingServiceName(this.subCategory.value, this.selectedAuthControlsData)
    }
    this.handleCancel();
  }
  getCookingServiceName(CookingServiceId, authControl: FormGroup) {
    this.finalSubCategoryNames = [];
    let cookingServices = []
    cookingServices = CookingServiceId
    cookingServices.forEach((data) => {
      let index = this.subCategoryList.findIndex((item, i) => item._id == data)
      if (index != -1) {
        this.finalSubCategoryNames.push(this.subCategoryList[index].name)
        return this.finalSubCategoryNames;

      } else {
        return []
      }
    })
    authControl.patchValue({
      Sub_Category_Names: this.finalSubCategoryNames
    })
  }

  onModal(skill) {
    console.log('skill: ', skill);
    this.isVisible = true;
    this.selectedAuthControlsData = skill;
    // if (this.isEditing) {}
    this.subCategory.patchValue(skill.value.Sub_Category_ID_Array)
  }
}
