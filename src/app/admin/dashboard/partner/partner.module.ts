import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PartnerRoutingModule } from './partner-routing.module';
import { PartnerComponent } from '../partner/partner.component';
import { basicImportsModule } from 'src/app/basicimports.module';


@NgModule({
  declarations: [PartnerComponent],
  imports: [
    CommonModule,
    basicImportsModule,
    PartnerRoutingModule
  ]
})
export class PartnerModule { }
