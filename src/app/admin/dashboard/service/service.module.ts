import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServiceRoutingModule } from './service-routing.module';
import { ServiceComponent } from '../service/service.component';
import { basicImportsModule } from 'src/app/basicimports.module';


@NgModule({
  declarations: [ServiceComponent],
  imports: [
    CommonModule,
    basicImportsModule,
    ServiceRoutingModule
  ]
})
export class ServiceModule { }
