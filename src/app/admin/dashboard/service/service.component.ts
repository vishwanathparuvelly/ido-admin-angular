import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.css']
})
export class ServiceComponent implements OnInit {

  selectedTab = 0;
  isTableLoading = false;
  selectedId = '';
  isVisible = false;
  categoryType = 1;
  name = '';
  sno = '';
  updateId: any;
  isVisibleUpdate = false;
  tableList = [];
  currentPage = 1;
  limit = this.appService.limit;
  nzPageSizeOptions = this.appService.nzPageSizeOptions;
  count: any = 0;
  accountParamsobj: any = {
    limit: this.limit,
    skip: this.currentPage
  };
  view = -1;
  fileInput = null;
  fileToUpload: File;
  description = '';
  available_for = '';
  quantityType = '';
  quantity = '';
  duration = '';
  base_price = '';
  subCategory = '';
  mainCategory = null;
  categoriesList: any[] = [];
  subCategorisList: any[] = [];
  typesList = [
    { name: 'M (Milligram)', value: 1 },
    { name: 'GM (Gram)', value: 2 },
    { name: 'KG (Kilogram)', value: 3 },
    { name: 'T (Tonne)', value: 4 },
    { name: 'GT (Gigatonne)', value: 5 }
  ];
  skip = 0;
  isEditing = false;
  availableFor = [
    { id: 0, name: 'Male' },
    { id: 1, name: 'Female' },
    { id: 2, name: 'Not Applicable' }
  ];
  ImageLink: any;
  ImageEditing = false;
  editId: string | Blob;
  isBtnLoading = false;
  subCategoryId = new FormControl('');
  mainCategoryId = new FormControl('');
  searchName = new FormControl('');

  constructor(
    public appService: AppService,
    public nzMessageService: NzMessageService
  ) { }

  ngOnInit() {
    this.getTableList();
    this.getCategories();
    this.mainCategoryId.valueChanges.subscribe(mc => {
      const mcv = mc;
      console.log('mcv: ', mcv);
      if (mcv !== null) {
        const ind = this.categoriesList.findIndex(c => c._id === mcv);
        this.subCategorisList = this.categoriesList[ind].subCategory;
      }
      this.limit = 10;
      this.currentPage = 1;
      this.skip = 0;
      this.getTableList();
    });
    this.subCategoryId.valueChanges.subscribe(sc => {
      const scv = sc;
      this.limit = 10;
      this.currentPage = 1;
      this.skip = 0;
      this.getTableList();
    });
    this.searchName.valueChanges.subscribe(sc => {
      const scv = sc;
      setTimeout(() => {
        this.limit = 10;
        this.currentPage = 1;
        this.skip = 0;
        this.getTableList();
      }, 500);
    });
  }

  getTableList(): any {
    this.isTableLoading = true;
    let url = 'api/admin/services';
    let body = { skip: this.skip, limit: this.limit,
      search_name: this.searchName.value,
      mainCategory: this.mainCategoryId.value,
      subCategory: this.subCategoryId.value };
    try {
      this.appService.postMethod(url, body)
        .subscribe((resp: any) => {
          console.log('resp ', resp);
          this.isTableLoading = false;
          this.tableList = resp.body.data.all_mainCategory;
          this.count = resp.body.data.totalCount;
        },
          (resp: any) => {
            console.log('err', resp);
            this.isTableLoading = false;
            this.nzMessageService.error(resp.error.message);
          });
    } catch (e) {
    }
  }

  onNextPage(event: number): any {
    console.log('next page ', event);
    this.view = -1;
    this.currentPage = event;
    this.skip = (this.currentPage - 1) * this.limit;
    this.getTableList();
  }

  onLimitChange(event: any): any {
    this.limit = event;
    this.currentPage = 1;
    this.skip = 0;
    this.getTableList();
  }

  create(type, data?): any {
    console.log('data: ', data);
    if (type === 1) {
      this.isVisible = true;
      this.isEditing = false;
      this.getCategories();
    } else if (type === 2) {
      this.isVisible = true;
      this.isEditing = true;
      this.description = data.description;
      this.name = data.name;
      this.sno = data.s_no;
      this.mainCategory = data.mainCategory._id;
      this.categorySelection(this.mainCategory);
      this.subCategory = data.subCategory._id;
      this.base_price = data.base_price;
      this.duration = data.duration;
      this.quantity = data.quantity;
      this.quantityType = data.quantityType;
      this.available_for = data.available_for;
      console.log('this.available_for: ', this.available_for);
      this.ImageLink = data.image;
      this.editId = data._id;
    }
  }

  cancel(): any { }

  confirmCategory(data: any): any {
    let url = 'api/admin/services/' + data._id;
    try {
      this.appService.deleteMethod(url)
        .subscribe((resp: any) => {
          console.log(resp);
          if (resp.status === 200) {
            this.nzMessageService.success(resp.body.data.message);
            this.skip = 0;
            this.getTableList();
          }
        },
          (resp: any) => {
            console.log('err', resp);
            this.nzMessageService.error(resp.error.message);
          });
    } catch (e) {
    }
  }

  handleOk() {
    this.isBtnLoading = true;
    let url = this.isEditing ? 'api/admin/services' : 'api/admin/services/create';
    const formData: FormData = new FormData();
    formData.append('s_no', this.sno);
    formData.append('name', this.name);
    formData.append('mainCategory', this.mainCategory);
    formData.append('subCategory', this.subCategory);
    formData.append('base_price', this.base_price);
    formData.append('duration', this.duration);
    formData.append('quantity', this.quantity);
    formData.append('quantityType', this.quantityType);
    formData.append('available_for', this.available_for.toString());
    formData.append('description', this.description);
    if (this.fileToUpload) {
      formData.append('image', this.fileToUpload);
    }
    if (this.isEditing) {
      formData.append('serviceId', this.editId);
    }
    try {
      if (!this.isEditing) {
        this.appService.fileMethod(url, formData)
          .subscribe((resp: any) => {
            console.log('resp ', resp);
            this.isVisible = false;
            this.isBtnLoading = false;
            if (resp.status === 200) {
              this.handleCancel();
              this.nzMessageService.success(resp.body.data.message);
              this.skip = 0;
              this.getTableList();
            }
          },
            (err: any) => {
              console.log('err', err);
              this.isBtnLoading = false;
              this.nzMessageService.error(err.error.message);
            });
      } else if (this.isEditing) {
        this.appService.filePutMethod(url, formData)
          .subscribe((resp: any) => {
            console.log('resp ', resp);
            this.isBtnLoading = false;
            this.isVisible = false;
            if (resp.status === 200) {
              this.handleCancel();
              this.nzMessageService.success(resp.body.data.message);
              this.skip = 0;
              this.getTableList();
            }
          },
            (err: any) => {
              console.log('err', err);
              this.isBtnLoading = false;
              this.nzMessageService.error(err.error.message);
            });
      }
    } catch (e) {
    }
  }

  handleCancel() {
    this.isVisible = false;
    this.sno = '';
    this.name = '';
    this.mainCategory = null;
    this.subCategory = '';
    this.base_price = '';
    this.duration = '';
    this.quantity = '';
    this.quantityType = '';
    this.available_for = '';
    this.description = '';
    this.fileInput = null;
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    console.log('fileToUpload ', this.fileToUpload); var reader = new FileReader();
    // this.imagePath = file;
    reader.readAsDataURL(this.fileToUpload);
    reader.onload = (_event) => {
      this.ImageLink = reader.result;
    }
    if (this.isEditing) {
      this.ImageEditing = true;
    }
  }

  categorySelection(event) {
    console.log('data: ', event);
    const ind = this.categoriesList.findIndex(c => c._id === event);
    this.subCategorisList = this.categoriesList[ind].subCategory;
  }

  getCategories(): any {
    this.accountParamsobj.limit = 0;
    this.accountParamsobj.skip = 0;
    let url = 'api/admin/mainCategory';
    try {
      this.appService.postMethod(url, this.accountParamsobj)
        .subscribe((resp: any) => {
          console.log('resp.body ', resp.body);
          this.categoriesList = resp.body.data.all_mainCategory;
        },
          (resp: any) => {
            console.log('err', resp);
            this.nzMessageService.error(resp.error.message);
          });
    } catch (e) {
    }
  }

}
