import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"],
})
export class DashboardComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  user = '';

  ngOnInit() {
    if (sessionStorage.getItem('superAdminData') !== null) {
      this.user = 'Super Admin';
    } else if (sessionStorage.getItem('adminData') !== null) {
      this.user = 'Admin';
    }
  }

  logOut() {
    sessionStorage.clear();
    this.router.navigateByUrl('/login');
  }
}
