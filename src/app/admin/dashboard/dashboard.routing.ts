import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';

const routes: Routes = [
    {
        path: '', component: DashboardComponent,
        children: [
            {
                path: 'superadmin',
                loadChildren: () => import('./../super-admin/super-admin.module').then(m => m.SuperAdminModule)
            },
            {
                path: 'city',
                loadChildren: () => import('./city/city.module').then(m => m.CityModule)
            },
            {
                path: 'categories',
                loadChildren: () => import('./categories/categories.module').then(m => m.CategoriesModule)
            },
            {
                path: 'services',
                loadChildren: () => import('./service/service.module').then(m => m.ServiceModule)
            },
            {
                path: 'extra',
                loadChildren: () => import('./extra/extra.module').then(m => m.ExtraModule)
            },
            {
                path: 'localities',
                loadChildren: () => import('./locality-section/locality-section.module').then(m => m.LocalitySectionModule)
            },
            {
                path: 'bookings',
                loadChildren: () => import('./bookings/bookings.module').then(m => m.BookingsModule)
            },
            {
                path: 'partners',
                loadChildren: () => import('./partner/partner.module').then(m => m.PartnerModule)
            },
            {
                path: 'users',
                loadChildren: () => import('./users/users.module').then(m => m.UsersModule)
            },
            { path: '', redirectTo: 'city', pathMatch: 'prefix' }
        ]
    }
];

@NgModule({
    imports: [CommonModule, RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DashboardRoutingModule { }
