import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-locality',
  templateUrl: './locality.component.html',
  styleUrls: ['./locality.component.css']
})
export class LocalityComponent implements OnInit {

  isTableLoading: boolean;
  tableList = [];
  selectedObj: any = {};
  myForm: FormGroup;
  Latitude: number = 17.383;
  Longitude: number = 78.3838;
  address_googlePlaces: string = '';
  isEditing: boolean;
  isBtnLoading: boolean;
  detailViewIndex: number;
  isFormVisiable = false
  city_SettingsObj: any = {};
  isModalVisiable: boolean;
  settingsForm: FormGroup;
  currentPage = 1;
  limit = this._appService.limit;
  nzPageSizeOptions = this._appService.nzPageSizeOptions;
  count: any = 0;
  accountParamsobj: any = {
    limit: this.limit,
    skip: this.currentPage
  };
  citiesList: any[] = [];
  cityId = '';
  view = -1;

  constructor(
    private nzMessageService: NzMessageService,
    private _appService: AppService
  ) { }

  ngOnInit(): void {
    this.settingsForm = new FormGroup({
      s_no: new FormControl(false, [Validators.required]),
      name: new FormControl(null, [Validators.required]),
      polygon_path: new FormControl(null, [Validators.required])
    })
    this.getCities();
  }

  getCities(): any {
    let url = 'api/admin/city';
    try {
      this._appService.postMethod(url, { skip: 0, limit: 10 })
        .subscribe((resp: any) => {
          console.log('resp ', resp);
          this.citiesList = resp.body.data.all_city;
          if (this.citiesList.length > 0) {
            this.cityId = this.citiesList[0]._id;
            this.getTableList();
          }
        },
          (resp: any) => {
            console.log('err', resp);
            this.nzMessageService.error(resp.error.message);
          });
    } catch (e) {
    }
  }

  getTableList(): any {
    this.accountParamsobj.limit = this.limit;
    this.accountParamsobj.skip = this.currentPage - 1;
    this.isTableLoading = true;
    let url = 'api/admin/city/locality/' + this.cityId;
    try {
      this._appService.postMethod(url, this.accountParamsobj)
        .subscribe((resp: any) => {
          console.log('resp ', resp);
          this.isTableLoading = false;
          this.tableList = resp.body.data.all_extras;
          this.count = resp.body.data.totalCount;
        },
          (resp: any) => {
            console.log('err', resp);
            this.isTableLoading = false;
            this.nzMessageService.error(resp.error.message);
          });
    } catch (e) {
    }
  }

  onNextPage(event: number): any {
    console.log('next page ', event);
    this.view = -1;
    this.currentPage = event;
    this.getTableList();
  }

  onLimitChange(event: any): any {
    this.limit = event;
    this.currentPage = 1;
    this.getTableList();
  }

  onGetAddForm() {
    this.isEditing = false;
    this.isFormVisiable = true;
    // setTimeout(() => {
    //   this.onMapIntilized()
    // }, 200);
  }

}
