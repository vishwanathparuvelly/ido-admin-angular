import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LocalityRoutingModule } from './locality-routing.module';
import { LocalityComponent } from '../locality/locality.component';
import { basicImportsModule } from 'src/app/basicimports.module';


@NgModule({
  declarations: [LocalityComponent],
  imports: [
    CommonModule,
    basicImportsModule,
    LocalityRoutingModule
  ]
})
export class LocalityModule { }
