import { Routes, RouterModule } from '@angular/router';
import { LocalitySectionComponent } from './locality-section.component';

const routes: Routes = [
  {
    path: '',
    component: LocalitySectionComponent, children: [
      {
        path: 'create',
        loadChildren: () => import('./create-locality/create-locality.module').then(m => m.CreateLocalityModule)
      },
      {
        path: 'view',
        loadChildren: () => import('./locality-list/locality-list.module').then(m => m.LocalityListModule)
      },
      {
        path:'',redirectTo:'view',pathMatch:'full'
      }
    ]
  },
];

export const LocalitySectionRoutes = RouterModule.forChild(routes);
