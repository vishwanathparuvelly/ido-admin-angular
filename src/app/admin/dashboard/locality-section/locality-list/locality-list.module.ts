import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LocalityListComponent } from './locality-list.component';
import { LocalityListRoutes } from './locality-list.routing';
import { basicImportsModule } from 'src/app/basicimports.module';
import { NzDescriptionsModule } from 'ng-zorro-antd/descriptions';
import { NzListModule } from 'ng-zorro-antd/list';

@NgModule({
  imports: [
    CommonModule,
    basicImportsModule,
    NzDescriptionsModule,
    NzListModule,
    LocalityListRoutes
  ],
  declarations: [LocalityListComponent]
})
export class LocalityListModule { }
