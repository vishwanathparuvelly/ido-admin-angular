import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd';
import { AppService } from 'src/app/app.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
declare var google: any
@Component({
  selector: 'app-locality-list',
  templateUrl: './locality-list.component.html',
  styleUrls: ['./locality-list.component.css']
})
export class LocalityListComponent implements OnInit {
  localityList = [];
  cityList = [];
  map: any;
  polygonPaths = [];
  citySelectionForm: FormGroup;
  isTableloading: boolean;
  latitude: any = 17.78;
  longitude: any = 78.8474;
  Locality_Title: any;
  selectedObj: any;
  isPathsUpdatingBtn: boolean;
  drawingManager: any;
  all_overlays = [];
  newpolygone: any;

  constructor(
    private fb: FormBuilder,
    private nzMessageService: NzMessageService,
    private _appService: AppService) { }
  ngOnInit() {
    this.getCityList()
    this.citySelectionForm = this.fb.group({
      CityID: [null, Validators.required]
    })
    this.citySelectionForm.get('CityID').valueChanges.subscribe((data: any) => {
      console.log('data: ', data);
      this.latitude = parseFloat(data.lat);
      this.longitude = parseFloat(data.lng);
      if (this.citySelectionForm.get('CityID').valid) {
        this.getLocalityList(2)
        this.onMapIntilized()
      }
    })
  }
  getCityList() {
    const body = { skip: 0, limit: 10 }
    try {
      this._appService.postMethod('api/admin/city', body)
        .subscribe(resp => {
          this.cityList = resp.body.data.all_city
          console.log(this.cityList)
          if (this.cityList.length > 0) {
            // this.getLocalityList()
            this.citySelectionForm.patchValue({
              CityID: this.cityList[0]
            })
          }
        },
          error => {

            this.nzMessageService.error(error.error.message)
          })
    } catch (e) {

    }
  }
  getLocalityList(type) {
    console.log('this.citySelectionForm.get(\'CityID\').value', this.citySelectionForm.get('CityID').value)
    let url = 'api/admin/city/locality'
    try {
      this._appService.postMethod(url, { skip: 0, limit: 10,cityId:  this.citySelectionForm.get('CityID').value['_id']})
        .subscribe(resp => {
          console.log('resp: ', resp.body.data);
          if (resp.body.data.all_Locality) {
            this.localityList = resp.body.data.all_Locality
            if (type == 2) {
              this.localityList.forEach((item) => {
                item.editable = false;
              });
              this.localityList.forEach((item, index) => {
                this.onSettingZonesOnMap(item.polygon_path, item);
              })
            }
          }
        },
          error => {
            this.nzMessageService.error(error.error.message);
          })
    } catch (e) {
    }
  }
  onSettingZonesOnMap(latlngs, zonedetails) {
    const sample = [];
    for (let z = 0; z < latlngs.length; z++) {
      sample.push(new google.maps.LatLng(parseFloat(latlngs[z].lat), parseFloat(latlngs[z].lng)));
    }
    const boundary = new google.maps.Polygon({
      paths: sample,
      strokeColor: '#000',
      strokeWeight: '',
      fillColor: '#000',
      fillOpacity: 10,
      zIndex: 1,
      editable: zonedetails.editable,
      content: 'title ' + zonedetails.name
    });
    boundary.set('Info', 'idy');
    boundary.setMap(this.map);
    const infoWindow = new google.maps.InfoWindow;

    boundary.addListener('click', (event) => {

      infoWindow.setContent(zonedetails.name);
      infoWindow.setPosition(event.latLng);
      infoWindow.open(this.map);
    });
    google.maps.event.addListener(boundary.getPath(), 'set_at', (event) => {

      this.getnewpahts(boundary.getPath())

    });
    google.maps.event.addListener(boundary.getPath(), 'insert_at', (event) => {

      this.getnewpahts(boundary.getPath())

    });
  }
  getnewpahts(polygon) {
    this.polygonPaths = []
    polygon.getArray().forEach((path, index) => {
      const line = {
        lat: path.lat(),
        lng: path.lng()
      }
      this.polygonPaths.push(line)
      var polygon1 = {
        draggable: true,
        editable: true,
        fillColor: "red",
        paths: this.polygonPaths
      };
    })
  }
  // onAction(data) {
  //   let url;
  //   if (data.Status) {
  //     url = 'Inactivate_Locality'
  //   } else {
  //     url = 'Activate_Locality'
  //   }
  //   try {
  //     const AdminData = JSON.parse(sessionStorage.getItem('AdminData'))
  //     const body = {
  //       AdminID: AdminData.AdminID,
  //       SessionID: AdminData.SessionID,
  //       LocalityID: data.LocalityID,
  //     }
  //     this._appService.postMethod(url, body)
  //       .subscribe(resp => {
  //         if (resp.success) {
  //           let msg;
  //           if (data.Status) {
  //             msg = 'Inactivated Successfully'
  //           } else {
  //             msg = 'Activated Successfully'
  //           }
  //           this.nzMessageService.success(msg)
  //           this.getLocalityList(1)
  //           this.onMapIntilized()
  //         } else {
  //         }
  //       },
  //         error => {
  //           this.nzMessageService.error(error.error.extras.msg)
  //         })
  //   } catch (e) { }
  // }
  onMapIntilized() {
    if (document.getElementById('map') != undefined) {
      this.map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: this.latitude, lng: this.longitude },
        zoom: 10,

        fullscreenControl: true
      });
    }


  }
  onEditLocality(localitydata) {
    console.log('localitydata: ', localitydata);
    this.selectedObj = localitydata
    this.polygonPaths = this.selectedObj.polygon_path;
    this.Locality_Title = this.selectedObj.name;
    this.latitude = this.polygonPaths[0].lat
    this.longitude = this.polygonPaths[0].lng
    this.onMapIntilized();
    this.localityList.forEach((item) => {
      if (item._id == this.selectedObj._id) {
        item.editable = true
      } else {
        item.editable = false
      }
    })
    this.localityList.forEach((item, index) => {
      this.onSettingZonesOnMap(item.polygon_path, item);
    })
    this.onSettingDrawingTools(true);
  }

  onUpdate() {
    this.isPathsUpdatingBtn = true;
    const url = 'api/admin/city/locality';
    try {
      const body: any = {
        s_no: this.selectedObj.s_no,
        city_id: this.selectedObj.city_id,
        locality_id: this.selectedObj._id,
        name: this.Locality_Title,
        polygon_path: this.polygonPaths
      }
      this._appService.putMethod(url, body)
        .subscribe(resp => {
          console.log('resp: ', resp);
            this.nzMessageService.success(resp.body.data.message);
            this.onMapIntilized()
            this.getLocalityList(1)
            this.isPathsUpdatingBtn = false;
            this.Locality_Title = ''
            this.polygonPaths = [];
        },
          error => {
            this.isPathsUpdatingBtn = false;
            this.nzMessageService.error(error.error.message);
          })
    } catch (e) { }
  }

  cancel() {}

  deleteLocality(data) {
    let url = 'api/admin/city/locality/' + data._id;
    try {
      this._appService.deleteMethod(url)
        .subscribe((resp: any) => {
          console.log(resp);
          if (resp.status === 200) {
            this.nzMessageService.success(resp.body.data.message);
            // this.currentPage = 1;
            // this.skip = 0;
            this.getLocalityList(2);
          }
        },
          (resp: any) => {
            console.log('err', resp);
            this.nzMessageService.error(resp.error.message);
          });
    } catch (e) {
    }
  }


  onSettingDrawingTools(setDrawingtool: boolean) {
    let polygon1 = {
      draggable: true,
      editable: true,
      fillColor: '#000'
    };
    const rect1 = {
      draggable: true,
      editable: true,
      fillColor: '#0f0'
    };
    this.drawingManager = new google.maps.drawing.DrawingManager({
      drawingMode: google.maps.drawing.OverlayType.POLYGON,
      drawingControl: setDrawingtool,
      polygonOptions: polygon1,

      drawingControlOptions: {
        position: google.maps.ControlPosition.TOP_CENTER,
        // drawingModes: ['polygon']
      }
    });
    this.drawingManager.setMap(this.map);
    google.maps.event.addListener(this.drawingManager, 'overlaycomplete', (event) => {
      // Polygon drawn
      this.all_overlays.push(event);
      this.newpolygone = event.overlay.getPath()
      this.polygonPaths = event.overlay.getPath().getArray();
      console.log('aaaaa' + JSON.stringify(this.polygonPaths))
      polygon1 = {
        draggable: true,
        editable: true,
        fillColor: '#f00'
      };
      if (event.type != google.maps.drawing.OverlayType.MARKER) {
        // Switch back to non-drawing mode after drawing a shape.
        this.drawingManager.setDrawingMode(null);
        // Add an event listener that selects the newly-drawn shape when the user
        // mouses down on it.
        const newShape = event.overlay;
        newShape.type = event.type;
        google.maps.event.addListener(newShape, 'click', (event) => {
          this.setSelection(newShape);
        });
        this.setSelection(newShape);
      }
      google.maps.event.addListener(this.drawingManager, 'drawingmode_changed', (event) => {
        this.clearSelection()
      });
      google.maps.event.addListener(this.newpolygone, 'set_at', (event) => {
        this.getnewpahts(this.newpolygone)
      });
      google.maps.event.addListener(this.newpolygone, 'insert_at', (event) => {
        this.getnewpahts(this.newpolygone)
      });
    });
  }

  setSelection(shape) {
  }

  clearSelection() {
  }

}
