import { Routes, RouterModule } from '@angular/router';
import { LocalityListComponent } from './locality-list.component';

const routes: Routes = [
  { path:'',
component:LocalityListComponent },
];

export const LocalityListRoutes = RouterModule.forChild(routes);
