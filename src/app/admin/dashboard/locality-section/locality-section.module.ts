import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LocalitySectionComponent } from './locality-section.component';
import { LocalitySectionRoutes } from './locality-section.routing';
import { basicImportsModule } from 'src/app/basicimports.module';

@NgModule({
  imports: [
    CommonModule,
    basicImportsModule,
    LocalitySectionRoutes
  ],
  declarations: [LocalitySectionComponent]
})
export class LocalitySectionModule { }
