import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd';
import { AppService } from 'src/app/app.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
declare var google: any
@Component({
  selector: 'app-create-locality',
  templateUrl: './create-locality.component.html',
  styleUrls: ['./create-locality.component.css']
})
export class CreateLocalityComponent implements OnInit {

  cityList = [];
  map: any;
  latitude = '17.38';
  longitude = '78.44';
  drawingManager: any;
  all_overlays = [];
  polygonPaths = [];
  newpolygone: any;
  myForm: FormGroup;
  isBtnLoading: boolean;
  localityList = [];

  constructor(
    private nzMessageService: NzMessageService,
    private _appService: AppService) { }

  ngOnInit() {
    this.getCityList()
    this.myForm = new FormGroup({
      CityID: new FormControl(null, Validators.required),
      Locality_Title: new FormControl(null, Validators.required),
      Locality_Serial: new FormControl(null, Validators.required),
      fillColor: new FormControl('#000', Validators.required)
    })
    this.myForm.get('CityID').valueChanges.subscribe((data: any) => {
      if (this.myForm.get('CityID').valid) {
        // this.latitude = data.Latitude
        // this.longitude = data.Longitude
        // console.log(this.latitude)

        setTimeout(() => {
          this.onMapIntilized()
          this.onSettingDrawingTools(true)
          this.getLocalityList()
        }, 200);

      }
    })

  }
  getCityList() {
    const body = { skip: 0, limit: 10 }
    try {
      this._appService.postMethod('api/admin/city', body)
        .subscribe(resp => {
          this.cityList = resp.body.data.all_city
          console.log(this.cityList)
          if (this.cityList.length > 0) {
            // this.getLocalityList()
          }
        },
          error => {

            this.nzMessageService.error(error.error.message)
          })
    } catch (e) {

    }
  }

  getLocalityList() {
    let url = 'api/admin/city/locality/' + this.myForm.get('CityID').value['_id']
    // let url = 'api/admin/city/locality/' + this.myForm.controls['CityID'].value.CityID
    try {
      this._appService.postMethod(url, { skip: 0, limit: 10,cityId:  this.myForm.get('CityID').value['_id'] })
        .subscribe(resp => {
          console.log('resp: ', resp.body.data);
          this.localityList = resp.body.data.all_Locality.localities;
          // if (type == 2) {
          this.localityList.forEach((item, index) => {
            item.Polygon_Properties.editable = false
            this.onSettingZonesOnMap(item.polygon_path, item);
          })
          // }
        },
          error => {
            this.nzMessageService.error(error.error.message);
          })
    } catch (e) {
    }
  }
  ngAfterViewInit(): void {

    this.onMapIntilized()
  }
  onMapIntilized() {
    let center = { lat: parseFloat(this.latitude), lng: parseFloat(this.longitude) }
    this.map = new google.maps.Map(document.getElementById('map'), {
      center: center,
      zoom: 10,

      fullscreenControl: true
    });

  }
  onSettingDrawingTools(setDrawingtool: boolean) {
    console.log(this.myForm.get('fillColor').value)
    let polygon1 = {
      draggable: true,
      editable: true,
      fillColor: this.myForm.get('fillColor').value
    };
    const rect1 = {
      draggable: true,
      editable: true,
      fillColor: '#0f0'
    };
    this.drawingManager = new google.maps.drawing.DrawingManager({
      drawingMode: google.maps.drawing.OverlayType.POLYGON,
      drawingControl: setDrawingtool,
      polygonOptions: polygon1,

      drawingControlOptions: {
        position: google.maps.ControlPosition.TOP_CENTER,
        // drawingModes: ['polygon']
      }
    });
    this.drawingManager.setMap(this.map);
    google.maps.event.addListener(this.drawingManager, 'overlaycomplete', (event) => {
      // Polygon drawn
      this.all_overlays.push(event);
      this.newpolygone = event.overlay.getPath()
      this.polygonPaths = event.overlay.getPath().getArray();
      console.log('aaaaa' + JSON.stringify(this.polygonPaths))
      polygon1 = {
        draggable: true,
        editable: true,
        fillColor: '#f00'
      };
      if (event.type != google.maps.drawing.OverlayType.MARKER) {
        // Switch back to non-drawing mode after drawing a shape.
        this.drawingManager.setDrawingMode(null);
        // Add an event listener that selects the newly-drawn shape when the user
        // mouses down on it.
        const newShape = event.overlay;
        newShape.type = event.type;
        google.maps.event.addListener(newShape, 'click', (event) => {
          this.setSelection(newShape);
        });
        this.setSelection(newShape);
      }
      google.maps.event.addListener(this.drawingManager, 'drawingmode_changed', (event) => {
        this.clearSelection()
      });
      google.maps.event.addListener(this.newpolygone, 'set_at', (event) => {
        this.getnewpahts(this.newpolygone)
      });
      google.maps.event.addListener(this.newpolygone, 'insert_at', (event) => {
        this.getnewpahts(this.newpolygone)
      });
    });
  }
  setSelection(shape) {

  }
  clearSelection() {

  }
  getnewpahts(polygon) {
    this.polygonPaths = []
    polygon.getArray().forEach((path, index) => {
      const line = {
        lat: path.lat(),
        lng: path.lng()
      }
      this.polygonPaths.push(line)
      var polygon1 = {
        draggable: true,
        editable: true,
        fillColor: "red",
        paths: this.polygonPaths
      };
    })
    console.log(this.polygonPaths)
  }

  onSubmitLocality() {
    this.isBtnLoading = true;
    let url = 'api/admin/city/locality/create';
    const body = {
      city_id: this.myForm.controls['CityID'].value._id,
      name: this.myForm.controls['Locality_Title'].value,
      polygon_path: this.polygonPaths,
      s_no: this.myForm.controls['Locality_Serial'].value
    }
    try {
      this._appService.postMethod(url, body)
        .subscribe(resp => {
          this.isBtnLoading = false;
          this.myForm.reset();
          this.onMapIntilized()
          this.onSettingDrawingTools(false)
          this.nzMessageService.success(resp.body.data.message);
        },
          error => {
            this.isBtnLoading = false
            this.nzMessageService.error(error.error.message);
          })
    } catch (e) {
    }
  }

  onSettingZonesOnMap(latlngs, zonedetails) {
    const sample = [];
    for (let z = 0; z < latlngs.length; z++) {
      sample.push(new google.maps.LatLng(parseFloat(latlngs[z].lat), parseFloat(latlngs[z].lng)));
    }
    const boundary = new google.maps.Polygon({
      paths: sample,
      strokeColor: zonedetails.Polygon_Properties.strokeColor,
      strokeWeight: zonedetails.Polygon_Properties.strokeWeight,
      fillColor: zonedetails.Polygon_Properties.fillColor,
      fillOpacity: zonedetails.Polygon_Properties.fillOpacity,
      zIndex: 1,
      editable: false,
      content: 'title ' + zonedetails.City_Title
    });
    boundary.set('Info', 'idy');
    boundary.setMap(this.map);
    const infoWindow = new google.maps.InfoWindow;

    boundary.addListener('click', (event) => {

      infoWindow.setContent(zonedetails.Locality_Title);
      infoWindow.setPosition(event.latLng);
      infoWindow.open(this.map);
    });
    // google.maps.event.addListener(boundary.getPath(), 'set_at', (event) => {

    //   this.getnewpahts(boundary.getPath())

    // });
    // google.maps.event.addListener(boundary.getPath(), 'insert_at', (event) => {

    //   this.getnewpahts(boundary.getPath())

    // });
  }
}
