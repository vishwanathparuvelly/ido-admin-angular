import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateLocalityComponent } from './create-locality.component';
import { CreateLocalityRoutes } from './create-locality.routing';
import { basicImportsModule } from 'src/app/basicimports.module';

@NgModule({
  imports: [
    CommonModule,
    basicImportsModule,
    CreateLocalityRoutes
  ],
  declarations: [CreateLocalityComponent]
})
export class CreateLocalityModule { }
