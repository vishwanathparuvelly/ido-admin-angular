import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AppService } from 'src/app/app.service';

declare var google: any;

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.css']
})
export class BookingsComponent implements OnInit {

  selectedTab = 0;
  isTableLoading = false;
  selectedId = '';
  isVisible = false;
  categoryType = 1;
  name = '';
  sno = '';
  updateId: any;
  isVisibleUpdate = false;
  tableList = [];
  currentPage = 1;
  limit = this.appService.limit;
  nzPageSizeOptions = this.appService.nzPageSizeOptions;
  count: any = 0;
  accountParamsobj: any = {
    limit: this.limit,
    skip: this.currentPage
  };
  view = -1;
  // serviceId = new FormControl();
  ServicesList = [];
  isFormVisiable = false;
  isEditing = false;
  myForm: FormGroup;
  isBtnLoading: boolean;
  serviceName = '';
  BookingData: any;
  mapOfExpandData: { [key: string]: boolean } = {};
  assignBookingVisible = false;
  citiesList: any[] = [];
  cityId = new FormControl('');
  partnersList = [];
  partner_id = new FormControl('');
  skip = 0;
  customer_id = new FormControl(null);
  service_id = new FormControl(null);
  payment_confirm = new FormControl(null);
  UsersList: any[] = [];
  map: any;
  lng = '78.44';
  lat = '17.38';

  constructor(
    public appService: AppService,
    public nzMessageService: NzMessageService
  ) { }

  ngOnInit() {
    this.getServices();
    this.getUsers();
    this.cityId.valueChanges.subscribe(ci => {
      const civ = ci;
      console.log('civ: ', civ);
      this.limit = 10;
      this.currentPage = 1;
      this.getPartners();
    });
    let center = { lat: parseFloat(this.lat), lng: parseFloat(this.lng) }
    this.map = new google.maps.Map(document.getElementById('map'), {
      center: center,
      zoom: 10,
      fullscreenControl: true
    });
  }

  getTableList(): any {
    this.isTableLoading = true;
    let url = 'api/admin/bookings';
    let body = {
      skip: this.skip, limit: this.limit,
      service_id: this.service_id.value !== null ? this.service_id.value : '',
      customer_id: this.customer_id.value !== null ? this.customer_id.value : '',
      payment_confirm: this.payment_confirm.value !== null ? (this.payment_confirm.value === 'yes' ? true : false ) : '',
      status: 0
    };
    try {
      this.appService.postMethod(url, body)
        .subscribe((resp: any) => {
          console.log('resp ', resp);
          this.isTableLoading = false;
          this.tableList = resp.body.data.allBooking;
          this.count = resp.body.data.totalCount;
        },
          (resp: any) => {
            console.log('err', resp);
            this.isTableLoading = false;
            this.nzMessageService.error(resp.error.message);
          });
    } catch (e) {
    }
  }

  onNextPage(event: number): any {
    console.log('next page ', event);
    this.view = -1;
    this.currentPage = event;
    this.skip = (this.currentPage - 1) * this.limit;
    this.getTableList();
  }

  onLimitChange(event: any): any {
    this.limit = event;
    this.currentPage = 1;
    this.skip = 0;
    this.getTableList();
  }

  cancel(): any { }

  confirmCategory(data: any) {
    let url = 'api/admin/services/extra/' + data.booking._id;
    try {
      this.appService.deleteMethod(url)
        .subscribe((resp: any) => {
          console.log(resp);
          if (resp.status === 200) {
            this.nzMessageService.success(resp.body.data.message);
            this.skip = 0;
            this.currentPage = 1;
            this.getTableList();
          }
        },
          (resp: any) => {
            console.log('err', resp);
            this.nzMessageService.error(resp.error.message);
          });
    } catch (e) {
    }
  }

  onExpandChange(index) {
    console.log('index: ', index);
    if (this.view !== index) {
      this.view = index;
      this.getAdminInfo(this.tableList[index]._id);
    } else if (this.view === index) {
      this.view = -1;
    }
  }

  getAdminInfo(id: any): any {
    let url = 'api/admin/bookings/' + id;
    try {
      this.appService.getMethod(url)
        .subscribe((resp: any) => {
          console.log('admin ', resp.data);
          this.BookingData = resp.data;
          // this.Permissions = resp.permissions;
        }, (err: any) => {
          console.log('err', err);
          this.nzMessageService.error(err.error.message);
        });
    } catch (e) {
    }
  }

  onAction(data, type: string) {
    let url = '';
    let body = {
      booking_id: data.booking._id
    };
    if (type === 'reject') {
      url = 'api/admin/bookings/reject';
    }
    try {
      this.appService.postMethod(url, body)
        .subscribe((resp: any) => {
          console.log(resp);
          if (resp.status === 200) {
            this.nzMessageService.success(resp.body.data.message);
            this.getTableList();
          }
        },
          (resp: any) => {
            console.log('err', resp);
            this.nzMessageService.error(resp.error.message);
          });
    } catch (e) {
    }
  }

  assignBookingCancel() {
    this.assignBookingVisible = false;
  }

  assign(data) {
    console.log('data: ', data);
    this.assignBookingVisible = true;
    this.BookingData = data;
    this.partner_id.reset();
    this.getCities();
  }

  assignBooking() {
    let url = '';
    url = 'api/admin/bookings/assign-partner';
    let body = { booking_id: this.BookingData._id, partner_id: this.partner_id.value };
    console.log('body: ', body);
    try {
      this.appService.putMethod(url, body)
        .subscribe(resp => {
          this.assignBookingCancel();
          this.nzMessageService.success(resp.body.data.message);
          this.getTableList();
        },
          (err: any) => {
            this.nzMessageService.error(err.error.message);
          });
    } catch (e) { }
  }

  getPartners(): any {
    this.accountParamsobj.limit = this.limit;
    this.accountParamsobj.skip = this.currentPage - 1;
    let body = {
      "search_input": '',
      "cityId": this.cityId.value,
      "localities": '',
      "subCategoryId": '',
      "mainCategoryId": '',
      "skip": this.accountParamsobj.skip,
      "limit": this.accountParamsobj.limit
    };
    let url = 'api/admin/partner';
    try {
      this.appService.postMethod(url, body)
        .subscribe((resp: any) => {
          console.log('resp ', resp.body);
          this.partnersList = resp.body.data.partner;
        },
          (resp: any) => {
            console.log('err', resp);
            this.nzMessageService.error(resp.error.message);
          });
    } catch (e) {
    }
  }

  getCities(): any {
    let url = 'api/admin/city';
    let body = { skip: this.skip, limit: this.limit }
    try {
      this.appService.postMethod(url, body)
        .subscribe((resp: any) => {
          console.log('resp ', resp);
          this.citiesList = resp.body.data.all_city;
          if (this.citiesList.length > 0) {
            this.cityId.setValue(this.citiesList[0]._id);
          }
        },
          (resp: any) => {
            console.log('err', resp);
            this.nzMessageService.error(resp.error.message);
          });
    } catch (e) {
    }
  }

  userStatus(event) {
    console.log('event: ', event);
    this.skip = 0;
    this.currentPage = 1;
    this.getTableList();
  }

  getServices(): any {
    let url = 'api/admin/services';
    try {
      this.appService.postMethod(url, { skip: 0, limit: 10 })
        .subscribe((resp: any) => {
          console.log('resp ', resp);
          this.ServicesList = resp.body.data.all_mainCategory;
          if (this.ServicesList.length > 0) {
            this.service_id.setValue(this.ServicesList[0]._id);
          }
        },
          (resp: any) => {
            console.log('err', resp);
            this.isTableLoading = false;
            this.nzMessageService.error(resp.error.message);
          });
    } catch (e) {
    }
  }

  getUsers(): any {
    let url = 'api/admin/users';
    try {
      this.appService.postMethod(url, { skip: 0, limit: 10 })
        .subscribe((resp: any) => {
          console.log('resp ', resp);
          this.UsersList = resp.body.data.users;
        },
          (resp: any) => {
            console.log('err', resp);
            this.isTableLoading = false;
            this.nzMessageService.error(resp.error.message);
          });
    } catch (e) {
    }
  }

  detailView(data, AI) {
    if (this.view !== AI) {
      this.view = AI;
      let url = 'api/admin/bookings/' + data._id;
      try {
        this.appService.getMethod(url)
          .subscribe(resp => {
            this.BookingData = resp.data.booking;
            this.lat = this.BookingData.address.lat;
            this.lng = this.BookingData.address.lng;
            let center = { lat: parseFloat(this.lat), lng: parseFloat(this.lng) }
            this.map = new google.maps.Map(document.getElementById('map'), {
              center: center,
              zoom: 10,
              fullscreenControl: true
            });
          },
            (err: any) => {
              console.log('err: ', err);
              this.nzMessageService.error(err.error.message);
            });
      } catch (e) { }
    } else {
      this.view = -1;
    }
  }
}
