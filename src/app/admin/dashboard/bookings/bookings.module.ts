import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BookingsRoutingModule } from './bookings-routing.module';
import { BookingsComponent } from '../bookings/bookings.component';
import { basicImportsModule } from 'src/app/basicimports.module';


@NgModule({
  declarations: [BookingsComponent],
  imports: [
    CommonModule,
    basicImportsModule,
    BookingsRoutingModule
  ]
})
export class BookingsModule { }
