import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { NzTableModule } from 'ng-zorro-antd/table';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzDescriptionsModule } from 'ng-zorro-antd/descriptions';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';

@NgModule({
    declarations: [],
    imports: [CommonModule,
        NzTableModule,
        NzUploadModule,
        NzPaginationModule,
        NzDatePickerModule,
        NzSelectModule,
        NzInputModule,
        NzIconModule,
        NzButtonModule,
        NzCheckboxModule,
        NzDividerModule,
        NzRadioModule,
        NzGridModule,
        NzNotificationModule,
        NzFormModule,
        FormsModule,
        ReactiveFormsModule,
        NzMessageModule,
        NzTabsModule,
        NzPopconfirmModule,
        NzSwitchModule,
        NzModalModule,
        NzDescriptionsModule,
        NzDropDownModule,
        NzToolTipModule
    ],
    exports: [NzGridModule,
        NzTableModule,
        NzUploadModule,
        NzPaginationModule,
        NzDatePickerModule,
        NzSelectModule,
        NzInputModule,
        NzIconModule,
        NzButtonModule,
        NzCheckboxModule,
        NzDividerModule,
        NzRadioModule,
        NzNotificationModule,
        NzFormModule,
        FormsModule,
        ReactiveFormsModule,
        NzTabsModule,
        NzMessageModule,
        NzPopconfirmModule,
        NzSwitchModule,
        NzModalModule,
        NzDescriptionsModule,
        NzDropDownModule,
        NzToolTipModule
    ],
    providers: [],
})
export class AccountSharedModule { }
