import { DOCUMENT } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  limit = 10;
  nzPageSizeOptions = [10, 25, 35, 45, 55, 100];
  InvestorID: any;
  isSkipEditing: boolean = false
  baseUrl = 'https://api.ido.in/'
  header: HttpHeaders;
  IdentityType = {
    1: 'Aadhaar',
    2: 'PAN Card',
    3: 'Driving License'
  };
  //  Upload_Url = environment.Upload_Url
  constructor(private httpService: HttpClient,
    private router: Router,
    @Inject(DOCUMENT) private document: any,
  ) { }
  // public readonly ImageUpload_Url = this.getURL(2);
  public readonly admin_Url = this.baseUrl

  private setHeaders(params) {
    let token
    if(sessionStorage.getItem('token')!=null){

       token = JSON.parse(sessionStorage.getItem('token'));
    }
    console.log('token', token)
    let reqData: any = {};
    if (token != null) {
      reqData = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: token
        },
      };
    }
    else {
      reqData = {
        headers: {
          'Content-Type': 'application/json',
          //   Authorization: `Bearer ${token}`
        },
      };
    }

    if (params) {
      const reqParams = {};
      Object.keys(params).map(k => {
        reqParams[k] = params[k];
      });
      reqData.params = reqParams;
    }
    return reqData;
  }
  getMethod(getUrl: string, Params?): Observable<any> {
    return this.httpService.get(this.baseUrl + getUrl, this.setHeaders(Params));
  }
  postMethod(posturl: string, body: any): Observable<any> {
    let headers;
    let token
    console.log('sessionStorage.getItem ', sessionStorage.getItem('token'));
    if (sessionStorage.getItem('token')!=null) {
       token = JSON.parse(sessionStorage.getItem('token'));
    }
    if (token) {
      headers = new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: token
      });
    } else {
      headers = new HttpHeaders({
        'Content-Type': 'application/json'
      });
    }
    return this.httpService.post(this.baseUrl + posturl, body, { headers: headers, observe: 'response' });
  }
  //   postMethodImage(posturl: string, body: any): any {
  //       return this.httpService.post(this.ImageUrl + posturl, body)
  //     }

  onUploadFile(req): Observable<any> {
    return this.httpService.request(req)
  }

  fileMethod(url: string, file): Observable<any> {
    const token = JSON.parse(sessionStorage.getItem('token'));
    if (sessionStorage.getItem('token')) {
      this.header = new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: token
      });
    }
    else {
      this.header = new HttpHeaders({
        'Content-Type': 'application/json'
      });
    }
    return this.httpService.post(this.baseUrl + url, file, {
      headers: new HttpHeaders({
        Authorization: token
      }), observe: 'response'
    });
  }

  filePutMethod(url: string, file): Observable<any> {
    const token = JSON.parse(sessionStorage.getItem('token'));
    if (sessionStorage.getItem('token')) {
      this.header = new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: token
      });
    }
    else {
      this.header = new HttpHeaders({
        'Content-Type': 'application/json'
      });
    }
    return this.httpService.put(this.baseUrl + url, file, {
      headers: new HttpHeaders({
        Authorization: token
      }), observe: 'response'
    });
  }

  deleteMethod(posturl: string): Observable<any> {
    const token = JSON.parse(sessionStorage.getItem('token'));
    if (token) {
      this.header = new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: token
      });
    }
    else {
      this.header = new HttpHeaders({
        'Content-Type': 'application/json'
      });
    }
    return this.httpService.delete(this.baseUrl + posturl, { headers: this.header, observe: 'response' });
  }

  putMethod(posturl: string, body: any): Observable<any> {
    const token = JSON.parse(sessionStorage.getItem('token'));
    if (token) {
      this.header = new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: token
      });
    }
    else {
      this.header = new HttpHeaders({
        'Content-Type': 'application/json'
      });
    }
    return this.httpService.put(this.baseUrl + posturl, body, { headers: this.header, observe: 'response' });
  }
}