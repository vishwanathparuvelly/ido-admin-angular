import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse,
} from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { tap, retry, catchError } from "rxjs/operators";
import { Injectable } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { environment } from "./env";

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  constructor(private ts: ToastrService) {}
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      retry(1),
      tap((event) => {
        if (event instanceof HttpResponse) {
          if (event.body && event.body.success) {
            if (event.url === `${environment.uploadUrl}/Upload_Document`) {
              this.ts.success(event.body.extras.Status);
              let fileUploadDocId = event["body"]["extras"]["DocumentID"];
              localStorage.setItem("documentId", fileUploadDocId);
            } else if (event.url === `${environment.uploadUrl}/Upload_Image`) {
              this.ts.success(event.body.extras.Status);
              let imageUploadId = event["body"]["extras"]["ImageID"];
              localStorage.setItem("imageId", imageUploadId);
            }
          }
        }
      }),
      catchError((error: HttpErrorResponse) => {
        console.log('error: ', error);
        let errorMessage = "";
        if (error.error instanceof ErrorEvent) {
          // client-side error
          errorMessage = `Error: ${error.error.message}`;
        } else {
          // server-side error
          errorMessage = `Error Code: ${error.status}\nMessage: ${error["error"]["extras"]["msg"]}`;
        }
        // window.alert(errorMessage);
        this.ts.error(errorMessage);
        return throwError(errorMessage);
      })
    );
  }
}
