import { Component, OnInit } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { Router } from "@angular/router";
import { AppService } from "../app.service";
import { NzMessageService } from "ng-zorro-antd/message";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  
  constructor(
    private fb: FormBuilder,
    private ts: ToastrService,
    private route: Router,
    public _appService: AppService,
    public nzMessageService: NzMessageService
  ) { }

  loginForm = this.fb.group({
    EmailID: ["", Validators.required],
    Password: ["", Validators.required],
    loginAs: ['A', Validators.required]
  });

  ngOnInit() { }

  onLogin() {
    for (const i in this.loginForm.controls) {
      this.loginForm.controls[i].markAsDirty();
      this.loginForm.controls[i].updateValueAndValidity();
    }
    const body = {
      email: this.loginForm.value.EmailID,
      password: this.loginForm.value.Password,
    }
    let url = '';
    if (this.loginForm.value.loginAs === 'A') {
      url = 'api/admin/login'
    } else if (this.loginForm.value.loginAs === 'B') {
      url = 'api/superadmin/login'
    }
    this._appService.postMethod(url, body)
      .subscribe((resp: any) => {
        console.log('resp', resp)
        if (resp.body.success) {
          console.log('resp', resp)
          if (this.loginForm.value.loginAs === 'A') {
            sessionStorage.setItem("adminData", JSON.stringify(resp.body.data.admin))
            sessionStorage.setItem("token", JSON.stringify(resp.body.data.admin.loginToken))
            this.route.navigateByUrl('/dashboard')
          } else if (this.loginForm.value.loginAs === 'B') {
            sessionStorage.setItem("superAdminData", JSON.stringify(resp.body.data.superadmin))
            sessionStorage.setItem("token", JSON.stringify(resp.body.data.superadmin.loginToken))
            this.route.navigateByUrl('/dashboard/superadmin')
          }
        } else {
          this.nzMessageService.error(resp.extras.msg)
        }
      },
        err => {
          console.log('err: ', err);
          this.nzMessageService.error(err.error.message);
        })
  }
}
