import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { LoginRoutingModule } from './login.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzRadioModule } from 'ng-zorro-antd/radio';

@NgModule({
    declarations: [
        LoginComponent
    ],
    imports: [
        CommonModule,
        NzRadioModule,
        LoginRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NzMessageModule
    ],
    exports: [],
    providers: [],
})
export class LoginModule { }